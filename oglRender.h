#ifndef _OGLRENDER_H
#define _OGLRENDER_H

#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <sstream>
#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <iomanip>
#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "dungeon.h"
#include "player.h"
#include "monster.h"

class oglRender {
public:
	oglRender();
	~oglRender();
	void Render();
	void setDungeon(Dungeon* dungeon, player_t* player, std::vector<character_t*>* m);
	void setRender(bool render);
	bool getRender();
	int stopWatch(std::chrono::system_clock::time_point t);
	GLFWwindow * getWindow();
	void setDebugMessage(std::string msg);
	std::string debugMessage;

	void setObjectMap(std::map<std::pair<int, int>, std::vector<object>>* map);
	void displayMenu(char i);
	void setItemSelection(int i) {
		itemSelection = i;
	}
	void setReveal(bool r) {
		_revealed = r;
	}

	void toggleCursorMode() {
		cursor_mode = !cursor_mode;
	}

	void setCursor(position_t c) {
		cursor = c;
	}
protected:

private:

	typedef struct dMsg {
		std::chrono::system_clock::time_point timestamp;
		std::string message;
	}dMsg;

	int initGLFW();
	void initOGL();
	void drawRectangle(GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLfloat h, std::vector<glm::vec4> &vertex_array);
	void addColor(glm::vec4 color, std::vector<glm::vec4> &color_array);
	void addTex(char tex, std::vector<glm::vec3> &tex_array);
	void editTex(int x, int y, char tex, std::vector<glm::vec3> &tex_array, GLuint texID, bool seen);
	std::string loadfile(const std::string &filename);
	
	GLuint VertexArrayID;
	GLuint dungeonPID;
	
	GLuint dungeonbuffer;
	std::vector<glm::vec4> g_dungeon_buffer_data;
	GLuint dungeonfrontcolorbuffer;
	std::vector<glm::vec4> g_dungeon_front_color_buffer_data;
	GLuint dungeonbackcolorbuffer;
	std::vector<glm::vec4> g_dungeon_back_color_buffer_data;
	GLuint dungeontexbuffer;
	std::vector<glm::vec3> g_dungeon_tex_buffer_data;
	std::vector<dMsg> dMsgList;

	void renderCharacters();
	GLuint characterbuffer;
	std::vector<glm::vec4> g_character_buffer_data;
	GLuint characterfrontcolorbuffer;
	std::vector<glm::vec4> g_character_frontcolor_buffer_data;
	GLuint characterbackcolorbuffer;
	std::vector<glm::vec4> g_character_backcolor_buffer_data;
	GLuint charactertexbuffer;
	std::vector<glm::vec3> g_character_tex_buffer_data;
	void renderDungeon();
	void initDebugWindow();
	void renderList();
	void swapBytes(int begin, int end, std::string &d);
	bool displayInventory;

	int dungeon_start;
	int dungeon_width;
	int dungeon_height;


	void renderPlayerStatus();

	bool _revealed;

	std::map<std::pair<int, int>, std::vector<object>>* object_map;


	GLuint loadShaders(std::string frag_name, std::string vert_name);
	void editColor(int x, int y, glm::vec4 color, std::vector<glm::vec4> &color_array, GLuint colorID);
	void editBuffer(int index, float w, float h, glm::vec4 position, std::vector<glm::vec4> &position_array, GLuint bufferID);

	std::vector<glm::vec4> basic_color;

	GLuint textureID;

	FT_Library ft;
	FT_Face face;
	FT_GlyphSlot g;
	GLuint debugtex;
	GLuint debugbuffer;
	GLuint debugPID;
	int itemSelection;
	void renderDebugWindow();

	position_t cursor;
	bool cursor_mode;

	GLFWwindow* window;
	Dungeon* d; 
	player_t* p;
	std::vector<character_t*>* mList; 
	bool _render;
	GLuint oglRender::loadImage(std::string _fn);

	std::chrono::time_point<std::chrono::high_resolution_clock> renderTime;
	std::chrono::time_point<std::chrono::system_clock> startTime;


	bool displayMonsterList;
	bool displayEquipment;
	unsigned int getElapsedTime();
	unsigned int getRenderTime();
};

#endif