#include "game.h"

int main(int argc, char * argv[]) {

	//Initialize Time for Rand

	//Seed the RNG

	srand(std::chrono::high_resolution_clock::now().time_since_epoch().count());


	int i;
	bool save = false;
	bool load = false;
	bool verbose_flag = false;
	bool player_auto = false;
	bool checkParser = false;
	std::string filename = std::string("dungeon");
	std::string mfilename = std::string("");
	std::string ofilename = std::string("");
	int Max_Rooms = 5;
	int p_speed = 10;
	int nummon = 5;
	//Argument Parser
	for (i = 1; i<argc; i++) {
		if (!strcmp(argv[i], "--save")) {
			save = true;
		}
		else if (!strcmp(argv[i], "--load")) {
			load = true;
		}
		else if (!strcmp(argv[i], "--nummon")) {
			if (i + 1<argc) {
				nummon = atoi(argv[i + 1]);
			}
		}
		else if (!strcmp(argv[i], "-s")) {
			if (i + 1<argc) {
				Max_Rooms = atoi(argv[i + 1]) > 5 ? atoi(argv[i + 1]) : 5;
			}
		}
		else if (!strcmp(argv[i], "-l")) {
			if (i + 1<argc) {
				if (filename[0] != '-' && strcmp(argv[i + 1], "")) {

					filename = std::string(argv[i + 1]);
				}
				else {
					printf("Invalid Load File, using dungeon\n");
					filename = std::string("dungeon");
				}
			}
		}
		else if (!strcmp(argv[i], "-lm")) {
			if (i + 1<argc) {
				if (filename[0] != '-' && strcmp(argv[i + 1], "")) {

					mfilename = std::string(argv[i + 1]);
				}
				else {
					printf("Invalid Load File, using default monster file\n");
					mfilename = std::string("monster_desc.txt");
				}
			}
		}
		else if (!strcmp(argv[i], "-lo")) {
			if (i + 1<argc) {
				if (filename[0] != '-' && strcmp(argv[i + 1], "")) {

					ofilename = std::string(argv[i + 1]);
				}
				else {
					printf("Invalid Load File, using default object file\n");
					ofilename = std::string("object_desc.txt");
				}
			}
		}
		else if (!strcmp(argv[i], "-pspeed")) {
			if (i + 1<argc) {
				p_speed = atoi(argv[i + 1]);
			}
		}
		else if (!strcmp(argv[i], "-v")) {
			verbose_flag = true;
		}
		else if (!strcmp(argv[i], "-auto")) {
			player_auto = true;
		}
		else if (!strcmp(argv[i], "-checkParser")) {
			checkParser = true;
		}
	}


	nummon = nummon>0 ? nummon : 1;
	p_speed = p_speed>0 ? p_speed : 10;



	
	//Game Loop;
	
	Game* game = new Game(filename, verbose_flag, load, save, player_auto, p_speed, nummon, checkParser, mfilename, ofilename, Max_Rooms);
	
	if (!checkParser && game->ParseIsGood) {
		game->gameLoop();
	}
	else {
		if (!checkParser) {
			std::cout << "Error: Either Object or Monster File is not good." << std::endl;
		}
	}
	
	delete game;
	

}