#include "util.h"

dice::dice(){
}
dice::~dice(){
}

dice::dice(std::string d_string) : dice(){
  int base_end = d_string.find("+");
  int num_end = d_string.find("d");
  _base = atoi(d_string.substr(0,base_end).c_str());
  _num_dice = atoi(d_string.substr(base_end+1,num_end-base_end-1).c_str());
  _sides = atoi(d_string.substr(num_end+1,d_string.length()-num_end-1).c_str());
  info = d_string;
  
}

int dice::roll(){

  int i;
  int total = 0;
  total+=_base;
  for(i = 0; i<_num_dice;i++){
    if(_sides != 0){
      total+=rand()%_sides+1;
    } 
  }
  return total;
  
}

int dice::min(){
  return _base+_num_dice;
}

int dice::max(){
return _base+_num_dice*_sides;  
}
position_t position_t::operator+(const position_t& right){
  position_t temp;
  temp._x = _x+right._x;
  temp._y = _y+right._y;
  return temp;  
}

position_t position_t::operator+(const dimension_t& right){
  position_t temp;
  temp._x = _x+right._w;
  temp._y = _y+right._h;
  return temp;  
}
position_t position_t::operator-(const position_t& right){
  position_t temp;
  temp._x = _x-right._x;
  temp._y = _y-right._y;
  return temp;  
}

position_t position_t::operator/(const int &s){
  position_t temp;
  temp._x = _x/s;
  temp._y = _y/s;
  return temp;  
}

bool position_t::operator==(const position_t& right){

  return (_x == right._x && _y == right._y);
}


position_t dimension_t::operator+(const dimension_t& right){
  position_t temp;
  temp._x = _w+right._w;
  temp._y = _h+right._h;
  return temp;  
}

position_t dimension_t::operator/(const int &s){
  position_t temp;
  temp._x = _w/s;
  temp._y = _h/s;
  return temp;  
}

int position_t::operator*(const position_t& right){
  return abs(_x*right._x)+abs(_y*right._y);
}


endianData endianSwap(endianData e){
  endianData final;
  final.str[0] = e.str[3];
  final.str[1] = e.str[2];
  final.str[2] = e.str[1];
  final.str[3] = e.str[0];
  return final;
}

bool checkEndian(){
  int i = 1;
  
  return (*(char *)&i == 1);
  
}


position_t getnewDirection(int d){
  int _DX[8]={-1,0,1,1, 1, 0,-1,-1};  
  int _DY[8]={ 1,1,1,0,-1,-1,-1, 0};
  position_t pos = position_t(0,0);
  if(d<8)
  pos = position_t(_DX[d],_DY[d]);
  return pos;
}

int getDirectionIndex(position_t delta){
  int _DX[8]={-1,0,1,1, 1, 0,-1,-1};  
  int _DY[8]={ 1,1,1,0,-1,-1,-1, 0};
  position_t pos = position_t(0,0);
  for(int i = 0; i<8;i++){
    if(delta.x() == _DX[i] && delta.y() == _DY[i]){ 
      return i;
    }
  }
  return 9;
}

int checkMonsterFields(std::string temp){

  	if(temp.find("NAME") == 0){
	  return NAME;
	}
	if(temp.find("DESC") == 0){
	  return DESC;
	}
	if(temp.find("COLOR") == 0){
	  return COLOR;
	}
	if(temp.find("SPEED") == 0){
	  return SPEED;
	}
	if(temp.find("ABIL") == 0){
	  return ABIL;
	}
	if(temp.find("HP") == 0){
	  return HP;
	}
	if(temp.find("DAM") == 0){
	  return DAM;
	}
	if(temp.find("SYMB") == 0){
	  return SYMB;
	}
	if(temp.find("BEGIN MONSTER") == 0){
	  return BEGIN_MONSTER;
	}
	if(temp.find("END") == 0){
	  return END;
	}
	return -1;
}

std::string getParsedData(std::string temp, std::string field, int &count){
    
    
    if(temp.length() > (field.length()+1)){
      return temp.substr((field.length()+1),temp.length()-(field.length()+1));
    }
    else{
     count = 2;
    }
  
  
  return std::string("Error");
}



int ability2index(std::string a){
  std::string abilArray[8] = {"SMART","TELE","TUNNEL","ERRATIC","PASS","PICKUP","DESTROY","UNIQ"};
  int ability = 0;
  std::stringstream abilityString(a);
  std::string abilityParse;
  while(std::getline(abilityString,abilityParse,' ')){
    for(int i = 0; i<8;i++){
      if(!std::string(abilArray[i]).compare(abilityParse))
	ability|=(0x1<<i);
    }
  }
  
  return ability;
}
/*
int ColorValueCompare(int a, int b){

  if(a<b) return COLOR_RED;
  if(a>b) return COLOR_GREEN;
  return COLOR_WHITE; 
}
*/
std::string index2ability(int i){
  int j;
  std::string out;
  for(j=0;j<8;j++){
    if((0x1<<j)&i){
      switch(j){
	case 0:
	  out += std::string("SMART ");
	break;
	case 1:
	out += std::string("TELE ");
	  break;
	case 2:
	out += std::string("TUNNEL ");
	  break;
	case 3:
	out += std::string("ERRATIC ");
	  break;
	case 4:
	out += std::string("PASS ");
	  break;
	case 5:
	out += std::string("PICKUP ");
	  break;
	case 6:
	out += std::string("DESTROY ");
	  break;
	case 7:
	out += std::string("UNIQ ");
	  break;
	default:
	  return std::string("ERROR");
	break;
      }
    }
  }
  return out;
  
}



int type2index(std::string a){
  std::string typeArray[20] = {"WEAPON","OFFHAND","RANGED","ARMOR","HELMET","CLOAK","GLOVES","BOOTS", "RING", "AMULET", "LIGHT", "SCROLL", "BOOK", "FLASK", "GOLD", "AMMUNITION", "FOOD", "WAND", "CONTAINER"};
  int type = 0;
    for(int i = 0; i<20;i++){
      if(!std::string(typeArray[i]).compare(a))
	type=(0x1<<i);
    }
  return type;
}
std::string index2type(int i){
  std::string typeArray[20] = {"WEAPON","OFFHAND","RANGED","ARMOR","HELMET","CLOAK","GLOVES","BOOTS", "RING", "AMULET", "LIGHT", "SCROLL", "BOOK", "FLASK", "GOLD", "AMMUNITION", "FOOD", "WAND", "CONTAINER"};
  int j;
  std::string out;
  for(j=0;j<20;j++){
    if((0x1<<j)&i){
      out+=typeArray[j];
    }
  }
  return out;
  
}



int color2index(std::string color){
  
  /*
        COLOR_BLACK   0
        COLOR_RED     1
        COLOR_GREEN   2
        COLOR_YELLOW  3
        COLOR_BLUE    4
        COLOR_MAGENTA 5
        COLOR_CYAN    6
        COLOR_WHITE   7
  */
  std::string colorArray[8] = {"BLACK","RED","GREEN","YELLOW","BLUE","MAGENTA","CYAN","WHITE"};
  int color_index = 0;
  
  std::stringstream colorString(color);
  std::string colorParse;
  while(std::getline(colorString,colorParse,' ')){
    for(int i = 0; i<8;i++){
      if(!std::string(colorArray[i]).compare(colorParse))
	color_index+=(0x1<<i);
    }
  }
  return color_index;
}

std::string index2color(int i){
  std::string out;
  for(int j = 0; j<8;j++){
    if((0x1<<j)&i){
    switch(j){
	case COLOR_BLACK:
	  out+=std::string("BLACK ");
	break;
	case COLOR_BLUE:
	  out+=std::string("BLUE ");
	  break;
	case COLOR_GREEN:
	  out+=std::string("GREEN ");
	  break;
	case COLOR_CYAN:
	  out+=std::string("CYAN ");
	  break;
	case COLOR_RED:
	  out+=std::string("RED ");
	  break;
	case COLOR_MAGENTA:
	  out+=std::string("MAGENTA ");
	  break;
	case COLOR_YELLOW:
	  out+=std::string("YELLOW ");
	  break;
	case COLOR_WHITE:
	  out+=std::string("WHITE ");
	  break;
	default:
	  break;
      }
    }
  }
  return out;
  
}



