#ifndef UTILS_H	
#define UTILS_H

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <vector>
#include <climits>
#include <string>
#include <map>
#include <thread>



#define MAX_H 80
#define MAX_V 21
class position_t;
class dimension_t;

enum mFields{
  NAME,
  DESC,
  SYMB,
  COLOR,
  SPEED,
  ABIL,
  HP,
  DAM,
  BEGIN_MONSTER,
  END
};

enum _COLOR {
	COLOR_BLACK,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_YELLOW,
	COLOR_BLUE,
	COLOR_MAGENTA,
	COLOR_CYAN,
	COLOR_WHITE
};

class dice{
  public:
  dice();
  ~dice();
  dice(std::string d_string);
  dice(int b, int n, int s) : _base(b), _num_dice(n),_sides(s){
  }
  int _base;
  int _num_dice;
  int _sides;
  std::string info;
  int roll();
  int max();
  int min();
};



class position_t{
public:
  int _x;
  int _y;
  position_t operator+(const position_t& right);
  position_t operator-(const position_t& right);
  position_t operator+(const dimension_t& right);
  position_t operator/(const int& right);
  int operator*(const position_t& right);
  bool operator==(const position_t& right);
  friend std::ostream& operator<<(std::ostream& out, const position_t &right){
    out<<right._x<< ", "<<right._y;
    return out;
  }
  std::pair<int,int> getKey(){
    return std::make_pair(_x,_y);
  }
  
  position_t(int x,int y){
    _x = x;
    _y = y;
      
  }
  position_t(){
    _x = 0;
    _y = 0;
  }
  ~position_t(){
  }
  
  int x(){
  return _x;
  }
  int y(){
  return _y;
  }
  
};

class dimension_t{

public:
  int _w;
  int _h;
  position_t operator+(const dimension_t& right);
  position_t operator/(const int& right);
  
  dimension_t(int w, int h){
    _w = w;
    _h = h;
  }
  dimension_t(){
    _w = 0;
    _h = 0;
  }
  ~dimension_t(){
  }
  
  int w(){
  return _w;
  }
  int h(){
  return _h;
  }
  
};

typedef union _Endiandata{
  char str[4];
  int integer;
} endianData;


endianData endianSwap(endianData e);
bool checkEndian();

position_t getnewDirection(int d);

int checkMonsterFields(std::string temp);
std::string getParsedData(std::string temp, std::string field, int &count);
int color2index(std::string color);
std::string index2color(int i);
int ability2index(std::string a);
std::string index2ability(int i);
int type2index(std::string a);
std::string index2type(int i);
int getDirectionIndex(position_t delta);
int ColorValueCompare(int a, int b);
#endif