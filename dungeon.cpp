#include "dungeon.h"
#include <sys/stat.h>



Dungeon::Dungeon(){
}


Dungeon::~Dungeon(){

  
}



std::vector<Room> Dungeon::getRooms(){
  return Rooms;
}

void Dungeon::init(){
  if(load){
    if(loadDungeon(filename) == -1){
      createDungeon();
    }
  }
  else{
    createDungeon();
  }
  
  if(save){
    saveDungeon(filename);
  }
}
char Dungeon::getSym(position_t pos){
  return dungeonMap[pos.y()][pos.x()];
}
Dungeon::Dungeon(int MR, bool s, bool l, std::string fn) : Dungeon(){
  Max_Rooms = MR;
  save = s;
  load = l;
  filename = fn;
  init();
}

int Dungeon::checkHardness(position_t p){
  return hardnessMap[p.y()][p.x()];
}
char Dungeon::checkMap(position_t p){
  return dungeonMap[p.y()][p.x()];
}
position_t Dungeon::getStairs(bool st){

  return (st?upStairs:downStairs);
  
}

void Dungeon::createTunnelMap(position_t p){
  int index = p.x()+p.y()*MAX_H;
  dijkstras(hardnessMap, index, index, true, true, parentTunnelMap);
}

void Dungeon::createNonTunnelMap(position_t p){
  int index = p.x()+p.y()*MAX_H;
  dijkstras(hardnessMap, index, index, true, false, parentNonTunnelMap);
}

position_t Dungeon::getNext(position_t p,bool tunnel){
  if(tunnel){
    return position_t(parentTunnelMap[p.y()][p.x()]%MAX_H,parentTunnelMap[p.y()][p.x()]/MAX_H);
  }
  else{
    return position_t(parentNonTunnelMap[p.y()][p.x()]%MAX_H,parentNonTunnelMap[p.y()][p.x()]/MAX_H);
  }
}

void Dungeon::decreaseHardness(position_t p,unsigned int amount){
  unsigned int currenthardnessMap = hardnessMap[p.y()][p.x()];
  
  if(currenthardnessMap>amount){
      currenthardnessMap-=amount;
      hardnessMap[p.y()][p.x()] = currenthardnessMap;
      createTunnelMap(p);
  }
  else{
      hardnessMap[p.y()][p.x()] = 0;
      dungeonMap[p.y()][p.x()] = '#';
      
      std::thread t1(&Dungeon::createTunnelMap, this, p);
      std::thread t2(&Dungeon::createNonTunnelMap, this, p);
      
      //createTunnelMap(p);
      //createNonTunnelMap(p);
      t1.join();
      t2.join();
  }
  
}


void Dungeon::renderDungeon(){
  int i, j;
      
      for(j = 0; j<MAX_V;j++){ 
	      for(i = 0; i<MAX_H;i++){
		      if(dungeonMap[j][i] == 0){
			      std::cout<< ' ';
		      }
		      else{
			      //if(dungeonMap[j][i] == '.' || dungeonMap[j][i] == '#')
			      
			      std::cout<<(char)dungeonMap[j][i];
		      }
	      }
		  std::cout << std::endl;
      }
	  std::cout << std::endl;

}

void Dungeon::DistanceMap(unsigned int Map[MAX_V][MAX_H]){
  int i, j;
      for(j = 0; j<MAX_V;j++){ 
	      for(i = 0; i<MAX_H;i++){
		  std::cout<< Map[j][i] % 10;
	      }
		  std::cout<<std::endl;
      }
	  std::cout << std::endl;

}

void Dungeon::drawBorders(bool mapExists){
	/*
  	int i;
  	//Make Bounds
	for(i = 0; i<MAX_V; i++){	  
	  if(hardnessMap[i][0] == 255){
	    dungeonMap[i][0] = '|';
	  }
	  
	  if(hardnessMap[i][MAX_H-1] ==255){
	    dungeonMap[i][MAX_H-1] = '|';
	  }
	  
	  if(!mapExists){
	    hardnessMap[i][0] = 255;
	    hardnessMap[i][MAX_H-1] = 255;
	    dungeonMap[i][0] = '|';
	    dungeonMap[i][MAX_H-1] = '|';
	  }
	  
	}

	for(i = 0; i<MAX_H; i++){
	  if(hardnessMap[0][i] == 255){
	    dungeonMap[0][i] = '-'; 
	  }
	  if(hardnessMap[MAX_V-1][i]==255){
	    dungeonMap[MAX_V-1][i] = '-';
	  }
	  
	  if(!mapExists){
	    hardnessMap[0][i] = 255;
	    hardnessMap[MAX_V-1][i] = 255;
	    dungeonMap[0][i] = '-';
	    dungeonMap[MAX_V-1][i] = '-';
	  }
	}
  */
  
}

void Dungeon::initDungeon(){
    std::memset(dungeonMap,0,sizeof(dungeonMap));
    std::memset(hardnessMap,0,sizeof(hardnessMap));
    std::memset(parentTunnelMap,0,sizeof(parentTunnelMap));
    std::memset(parentNonTunnelMap,0,sizeof(parentNonTunnelMap));
    std::memset(playerDestinationMap,0,sizeof(playerDestinationMap));
}

void Dungeon::initHardness(){
	int i, j;
	//Initialize the Hardness Map. For now make it random hardness.
	for(j = 0;j<MAX_V;j++){
		for(i=0;i<MAX_H;i++){
		    hardnessMap[j][i] = (rand() % 253) +1; 
		}
		
	}
}

void Dungeon::createRooms(){

  	//Create Room Positions
	//Bound Width is equal to the Max Bounds
	//subtracted by the left and right wall 
	//and a buffer of 2 offsets.
	//The reasoning is for room drawable space.
	int BoundWidth = MAX_H -4;
	int BoundHeight = MAX_V - 4;

	//Set Max Room Width and Height
	int MaxRoomWidth = 7;
	int MaxRoomHeight = 8;
	int MinRoomWidth = 3;
	int MinRoomHeight = 2;

	int offset = 2;
	
	int i;
	

	
	//Start Random Generation of Rooms
	for(i = 0; i<Max_Rooms;i++){
		Room room;
		int x = rand()%BoundWidth + offset;
		int y = rand()%BoundHeight + offset;
		int w = rand()%MaxRoomWidth + MinRoomWidth; 
		int h = rand()%MaxRoomHeight + MinRoomHeight;

		room = Room(x,y,w,h);
		//Initially say the room isn't valid, to run through loop.

		//Debug Check Top Left, Center, Bottom Right;
		int attempt = 0;
		while((room.right()>(MAX_H-2)) || 
			(room.bottom()>(MAX_V-2))||
			!room.getValid()){

			
			int intersectioncount = 0;
			
			x = rand()%BoundWidth + offset;
			y = rand()%BoundHeight + offset;
			w = rand()%MaxRoomWidth + MinRoomWidth; 
			h = rand()%MaxRoomHeight + MinRoomHeight;	
			
			room = Room(x,y,w,h);
			std::vector<Room>::iterator itr;
			
			for(itr = Rooms.begin(); itr != Rooms.end() ; itr++){
		
				if((*itr).checkRoomIntersection(room)){
					room.valid = false;
					intersectioncount++;

					//Debug Check for Intersection and with what.
					//printf("attempt %d : Room Intersection with Room %d and %d and with %d rooms.\n",attempt,k,i,intersectioncount);


					attempt++;
				}
			}
			if(intersectioncount == 0){			
				room.valid = true;
			}

			if(attempt > 4999){
				printf("Not enough space for new room, Max Rooms is now : %d.\n", i-1);
				Max_Rooms = Rooms.size();
				break;		
			}
			
		}
		if(room.valid){
		Rooms.push_back(room);
		}	
	}
}

void Dungeon::drawRooms(){
	
  
	std::vector<Room>::iterator itr;
  	//Draw Rooms
	for( itr = Rooms.begin(); itr!=Rooms.end();itr++){
		int m;
		int n;
		for(m = (*itr).x(); m<(*itr).right();m++){
			for(n = (*itr).y(); n<(*itr).bottom();n++){

				
				if ((*itr).x() == m  && dungeonMap[n][m] != '#'&& dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 4;
				}
				else if ((*itr).right()-1 == m && dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 6;
				}
				else if ((*itr).y() == n  && dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 2;
				}
				else if ((*itr).bottom() - 1 == n && dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 8;
				}
				else if ((*itr).x() == m && (*itr).y() == n  && dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 1;
				}
				else if ((*itr).right() - 1 == m && (*itr).y() == n && dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 3;
				}
				else if ((*itr).x() == m && (*itr).bottom() - 1 == n  && dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 7;
				}
				else if ((*itr).right() - 1 == m && (*itr).bottom() - 1 == n && dungeonMap[n][m] != '#') {
					dungeonMap[n][m] = 9;
				}
				else {
					dungeonMap[n][m] = '.';
				}
				
				hardnessMap[n][m] = 0;

			}

		}
		
		//Draw Walls

	}

  


}



void Dungeon::drawRoomsDemo() {


	std::vector<Room>::iterator itr;
	for (int i = 0; i < 80; i++) {
		for (int j = 0; j < 21; j++) {
			if (hardnessMap[j][i] == 0) {
				dungeonMap[j][i] = '#';
			}
		}
	}


	//Draw Rooms
	for (itr = Rooms.begin(); itr != Rooms.end(); itr++) {
		int m;
		int n;
		for (m = (*itr).x(); m<(*itr).right(); m++) {
			for (n = (*itr).y(); n<(*itr).bottom(); n++) {
					dungeonMap[n][m] = '.';
					hardnessMap[n][m] = 0;

			}

		}

		//Draw Walls

	}




}

void Dungeon::drawCorridors(){
	
	std::vector<Room>::iterator itr;
	//Draw Paths To Rooms
	for(itr = Rooms.begin();itr!=Rooms.end()-1;itr++){
		//Copy the Current Hardness Map.
		unsigned int tempHardnessMap[MAX_V][MAX_H];
		memcpy(tempHardnessMap,hardnessMap,sizeof(tempHardnessMap));
		
		int m, n;
		
		std::vector<Room>::iterator itr1;
		//For All Other Rooms, set hardness map as if it was a wall.
		for( itr1 = Rooms.begin(); itr1!=(Rooms.end()-1);itr1++){
			if(itr1!=itr && itr1!=(itr+1)){
				for(m = (*itr1).x(); m<(*itr1).right();m++){
					for(n = (*itr1).y(); n<(*itr1).bottom();n++){
						if(m>0 && m<MAX_H && n>0 && n<MAX_V){
							tempHardnessMap[n][m] = 255;
						}
					}
				}
			}
		}
		//Convert to 1D array for dijkstra's
		int i_start = (*itr).cx() + (*itr).cy()*MAX_H; 
		int i_end = (*(itr+1)).cx() + (*(itr+1)).cy()*MAX_H;
		//Calculate Shortest Path via Dijkstra's Algorithm.
		std::vector<int> path;
		int parray[MAX_V][MAX_H];
		dijkstras(tempHardnessMap, i_start, i_end, false, false, parray);
		
		int i = i_end;
		int x = i % MAX_H;
		int y = i / MAX_H;
		while(parray[y][x] != i_start && i>0 && i<1680){

			dungeonMap[y][x] = '#';
			hardnessMap[y][x] = 0;
			i = parray[y][x];
			x = i % MAX_H;
			y = i / MAX_H;
		}

	}
	
}


void Dungeon::loadCorridors(){
  int i,j;
  for(j = 0; j<MAX_V; j++){
    for(i = 0; i<MAX_H;i++){
      if(hardnessMap[j][i] == 0){
	dungeonMap[j][i] = '#';
      }      
    }
  }
  
}

int Dungeon::createDungeon(){

	//Reset All Parameters of the Dungeon
	initDungeon();
	Rooms.clear();
	
	initHardness();
	drawBorders(false);
	createRooms();
	drawCorridors();
	drawRooms();
	
	placeStairs(0);
	placeStairs(1);
	
	while(upStairs==downStairs){
	  placeStairs(1);
	}
	drawStairs();
	
	return Max_Rooms;
}


int Dungeon::loadDungeon(std::string fname){
  std::string file = std::string(fname);
  std::ifstream loadfile(file.c_str(), std::ifstream::binary);

  if(!loadfile.good()){
	  printf("File Error : %s\n", strerror(errno));
	  std::cout << file << std::endl;
    return -1;
    
  }
  
  
  //Initialize Dungeon Map
  initDungeon();
  
  std::vector<char> file_data((std::istreambuf_iterator<char>(loadfile)),
			      (std::istreambuf_iterator<char>())
		    );
  int max_file_size = file_data.size();
  //printf("File Size is %d\n",max_file_size);
  //printf("Read %d\n",file_data.strlen());

  char marker[6];
  endianData version;
  endianData size;
  
  int pos = 0;
  std::copy(file_data.begin(),file_data.begin()+6,marker);
  //printf("Header: %s\n",marker);
  pos+=sizeof(marker);
  
  std::copy(file_data.begin()+pos,file_data.begin()+pos+sizeof(int),version.str);
  pos+=sizeof(version.str);
  if(checkEndian()){
    version = endianSwap(version);
  }
  //printf("Version: %d\n", version.integer);
  
  
  std::copy(file_data.begin()+pos,file_data.begin()+pos+sizeof(int),size.str);
  pos+=sizeof(size.str);
  if(checkEndian()){
    size = endianSwap(size);
  }
  
  //printf("file_size: %d\n", size.integer);  
  
  int i;
  for(i = 0;i<(MAX_H*MAX_V);i++){
      hardnessMap[i/MAX_H][i%MAX_H] = (int)(unsigned char)(file_data[i+pos]);
  }
  pos+=(MAX_V*MAX_H);
  
  Max_Rooms = (max_file_size - pos) / 4;
  //printf("There are %d rooms.\n", Max_Rooms);
  
  Rooms.clear();
  for(i = 0; i<Max_Rooms; i++){
    
    int y = (int)file_data[pos+i*4];
    int x = (int)file_data[pos+i*4+1];
    int h = (int)file_data[pos+i*4+2];
    int w = (int)file_data[pos+i*4+3];
    Room room = Room(x,y,w,h);  
    //Initially say the room isn't valid, to run through loop.
    room.valid = false;
    Rooms.push_back(room);
  }

  
  
	
	drawBorders(true);
	drawRoomsDemo();

	placeStairs(0);
	placeStairs(1);

	while (upStairs == downStairs) {
		placeStairs(1);
	}
	drawStairs();

	return Max_Rooms;
}



int Dungeon::saveDungeon(std::string fname){

  
  unsigned char hardnessmap[MAX_V][MAX_H];
  int i,j;

  for(j=0;j<MAX_V;j++){
    for(i=0;i<MAX_H;i++){
      hardnessmap[j][i] = (unsigned char)(hardnessMap[j][i]);
    }
  }
  
  
  std::string file =  std::string(fname);
  //mkdir(path.c_str(),0777);

  
  char mark[7] = "RLG327";
  
  endianData version;
  version.integer= 0000;

  endianData size;
  size.integer = (int)(sizeof(int)+sizeof(version)+6+sizeof(hardnessmap) + Max_Rooms*4);
  //printf("Total Data to be Written: %d\n", size.integer);
  if(!checkEndian()){
  size = endianSwap(size);
  version = endianSwap(version);
  }
  
  std::ofstream savefile(file.c_str(), std::ifstream::binary);

  if(!savefile){
    printf("File Error : %s", strerror(errno));
    return Max_Rooms;
  }

   savefile<<mark;
   savefile.write(version.str,sizeof(int));
   savefile.write(size.str,sizeof(int));
   savefile.write((char *) hardnessmap,sizeof(hardnessmap));
  
   
   
  std::vector<Room>::iterator itr;
  for(itr=Rooms.begin();itr!=Rooms.end();itr++){
    endianData roomdata;
    roomdata.str[0]=(unsigned char)(*itr).y();
    roomdata.str[1]=(unsigned char)(*itr).x();
    roomdata.str[2]=(unsigned char)(*itr).h();
    roomdata.str[3]=(unsigned char)(*itr).w();
    
    savefile.write(roomdata.str, sizeof(roomdata.str));
  }

  
return 0;
}

bool Dungeon::checkStairs(position_t pos){
return (upStairs == pos || downStairs == pos);
}


void Dungeon::placeStairs(int type){

  std::vector<Room> Rooms = getRooms();
  int randomRoom = rand()%Rooms.size();
  position_t pos = Rooms.at(randomRoom).getRandomPosition();
  if(type){
    upStairs = pos;  
  }
  else{
    downStairs = pos;  
  }
}


void Dungeon::drawStairs(){
  
  dungeonMap[downStairs.y()][downStairs.x()] = '<';
  dungeonMap[upStairs.y()][upStairs.x()] = '>';
}

