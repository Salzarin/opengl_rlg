#ifndef MONSTER_H
#define MONSTER_H

#include "character.h"
#include "player.h"
#include "util.h"
#include<vector>

class monster_t : public character_t{
public:
  monster_t();
  ~monster_t();
  monster_t(player_t* p,  Dungeon * dungeon, int spd);
  monster_t(std::string name,std::string desc,std::string symbol,std::string color, std::string s, std::string abil,std::string hp,std::string dam);
  bool initMonsterPosition(bool mMap[MAX_V][MAX_H]);
  void initMonsterAttr();
  void setRandomPos();
  player_t * player;
  position_t dest;
  void monsterNextMove();
  void moveMonster();
  bool checkLos();
  void parseMonster();
  void setGame(player_t* p,Dungeon * dungeon);
  
  
  bool seen;
  Dungeon* d;
private:
  
  
};



#endif