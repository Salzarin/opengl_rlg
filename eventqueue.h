#ifndef HEAP_H
#define HEAP_H
#include <vector>
#include <queue>
#include <deque>
#include <algorithm>


#include "character.h"

class eventqueue{
public:
  std::vector<character_t*> heap;
  
  
  eventqueue(){
  }
  eventqueue(int i) : eventqueue(){
    heap.reserve(i);
  }  
  ~eventqueue(){

  }
  unsigned int left(unsigned int i){

    return (2*i+1);
  }

  unsigned int right(unsigned int i){

    return (2*i+2);
  }

  unsigned int parent(unsigned int i){
    return (i-1)/2;
  }
  void push(character_t* data){
    heap.push_back(data);
  }
  
  
  character_t* pop(){
	  character_t* temp = nullptr;
    if(!heap.size()){
      return temp;
    }
    temp = heap[0];
    std::iter_swap(heap.begin(), heap.end()-1);
    heap.pop_back();
    Heap(0);
    return temp;
  }
  
  void Heap(unsigned int check){
    unsigned int l = left(check);
    unsigned int r = right(check);
    unsigned int index = check;
    
    if(l<heap.size() && character_t()(heap[l],heap[check])){//&& Heap[l].dist < Heap[check].dist){
      index = l;
    }
    if(r<heap.size() && character_t()(heap[r],heap[index])){
      index = r;
    }
    if(index!=check){
	std::iter_swap(heap.begin()+check, heap.begin()+index);
	Heap(index);
    }
    
  }
  
  void buildHeap(){
    typename std::vector<character_t*>::reverse_iterator itr;
    for(itr = heap.rbegin();itr!=heap.rend();itr++){
      Heap(heap.rend()-itr-1);
    }
  }

  void decreaseKey(int val){
	  typename std::vector<character_t*>::iterator itr;
	  for (itr = heap.begin(); itr != heap.end(); itr++) {
		  (*itr)->decreaseEventTime(val);
	  }
  }
  void clear() {
	  heap.clear();
  }
};



#endif
