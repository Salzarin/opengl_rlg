#ifndef GAME_H
#define GAME_H


#include <cstdlib>
#include <chrono>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <thread>
#include <mutex>

#include "dungeon.h"
#include "player.h"
#include "monster.h"
#include "object.h"
#include "eventqueue.h"

#include "oglrender.h"

class Flags{
  public:
  int _NumberMonsters;
  int _Max_Rooms;
  bool _save;
  bool _load;
  bool _player_auto;
  int _player_speed;
  std::string _filename;
  bool _verbose;
  Flags();
  ~Flags();
  Flags(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms);
  Flags(const Flags& f);
};


class Game{
  


public:
  Game();
  Game(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon,bool checkParser,std::string mfilename, std::string ofilename, int max_rooms);
  ~Game();
  void gameLoop();
  bool ParseIsGood;
  void setKey(int _k);
  
private:

  bool checkParse;
  std::string _mFile;
  std::string _oFile;
  void gameInit();
  unsigned int getElapsedTime();
  unsigned int getRenderTime();
  unsigned int getLoopTime();
  Dungeon *d;
  player_t* p;
  bool _reveal;
  std::vector<character_t*>* mList;
  std::vector<monster_t> monsterTypes;
  std::vector<object> objectTypes;
  std::chrono::time_point<std::chrono::system_clock> startTime;
  std::chrono::time_point<std::chrono::high_resolution_clock> renderTime;
  std::chrono::time_point<std::chrono::high_resolution_clock> loopTime;
  void initAllColors();
  Flags* flag;
  void generateMonsters();
  void deleteMonsters();
  void renderMonsters();
  void resetEventList();
  void GameOverScreen();
  void printMonsterList();
  void resetMap();
  void generateObjects();
  void renderObjects();
  void renderMap();
  void teleportMode();
  void Render();
  void listInput();
  object popObject(position_t pos);
  
  bool BossDead;
  unsigned int monstersDead;
  bool teleporting;
  bool displayMonsterList;
  bool Rendering;
  int parseFile(std::string mfilename, std::string ofilename);
  int parseMonsters(std::string mfilename);
  int parseObjects(std::string ofilename);
  eventqueue eventQueue;
  position_t teleport;
  int cursorLocation;
  int listStart;
  int listSelect;
  int mList_W;
  int mList_H;
  std::mutex* mtx;
  
  void listItems(int type);
  bool displayItemList;
  void printItemList();
  bool displayItemDescription;
  void printItemDescription();
  
  void listEquipment();
  bool displayEquipmentList;
  void printEquipmentList();
  
  typedef std::map<std::pair<int,int>,character_t*> cMap;
  typedef std::pair<std::pair<int,int>,character_t*> cPair; 
  typedef std::pair<cMap::iterator,bool> cMapItr; 
  cMap character_map;
  cMapItr insertCharacter(position_t pos, character_t* c);
  void eraseCharacter(position_t pos);
  std::map<std::pair<int, int>, character_t*>::iterator checkCharacter(position_t pos);

  typedef std::map<std::pair<int,int>,std::vector<object>> oMap; 
  typedef std::pair<std::pair<int,int>,std::vector<object>> oPair;
  typedef std::pair<oMap::iterator,bool> oMapItr;
  oMap object_map;
  oMapItr insertObject(position_t pos, object obj);
  std::vector<object> eraseObject(position_t pos);
  oMap::iterator checkObject(position_t pos);
  bool godMode;
  
  void lookMode();
  bool monsterLook;
  bool displayMonsterDescription;
  position_t LookCursor;
  monster_t* character_desc;
  void printMonsterDescription();
  bool displayEquipment;
  bool _revealed;

  GLFWwindow* window;
  std::thread renderThread;

  int getKey();
  int _key;
  int playerCommand(int k);

  oglRender* renderingThread;
  int list_size;

protected: 
  
  
};
void charParse(GLFWwindow* window, unsigned int codepoint, int mods);
void keyParse(GLFWwindow* window, int key, int scancode, int action, int mods);
#endif