#include "oglRender.h"

oglRender::oglRender() {
	_render = false;
	displayInventory = false;
	_revealed = false;
	cursor_mode = false;
	displayMonsterList = false;
	displayEquipment = false;
	itemSelection = 0;
	basic_color.push_back(glm::vec4(0.0, 0.0, 0.0, 1.0));
	basic_color.push_back(glm::vec4(1.0, 0.0, 0.0, 1.0));
	basic_color.push_back(glm::vec4(0.0, 1.0, 0.0, 1.0));
	basic_color.push_back(glm::vec4(1.0, 1.0, 0.0, 1.0));
	basic_color.push_back(glm::vec4(0.0, 0.0, 1.0, 1.0));
	basic_color.push_back(glm::vec4(1.0, 0.0, 1.0, 1.0));
	basic_color.push_back(glm::vec4(0.0, 1.0, 1.0, 1.0));
	basic_color.push_back(glm::vec4(1.0, 1.0, 1.0, 1.0));
}

oglRender::~oglRender() {
}

int oglRender::stopWatch(std::chrono::system_clock::time_point t) {
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - t).count();
}
unsigned int oglRender::getElapsedTime() {
	return (int)std::chrono::duration_cast<std::chrono::milliseconds>
		(std::chrono::system_clock::now() - startTime).count();
}

unsigned int oglRender::getRenderTime() {
	return (int)std::chrono::duration_cast<std::chrono::microseconds>
		(std::chrono::high_resolution_clock::now() - renderTime).count();
}

void oglRender::displayMenu(char i) {
	std::string check("iem");
	int position = check.find(i);
	switch (position) {
	case 0:
		displayInventory = !displayInventory;
		break;
	case 1:
		displayEquipment = !displayEquipment;
		break;
	case 2:
		displayMonsterList = !displayMonsterList;
		break;
	default:
		break;
	}

}

void oglRender::setDungeon(Dungeon* dungeon, player_t* player, std::vector<character_t*>* m) {
	d = dungeon;
	p = player;
	mList = m;
	_render = false;
}

bool oglRender::getRender() {
	return _render;
}

void oglRender::setObjectMap(std::map<std::pair<int, int>, std::vector<object>>* map) {
	object_map = map;
}

int oglRender::initGLFW() {

	if (!glfwInit()) {
		std::cout << "Failed to initalize GLFW" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	window = glfwCreateWindow(1920, 1280, "RLG327", NULL, NULL);

	if (!window) {
		std::cout << "Failed to open GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		std::cout << "failed to initalize GLEW" << std::endl;
		return -1;
	}
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_FALSE);
	return 0;
}
void oglRender::drawRectangle(GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLfloat h, std::vector<glm::vec4> &vertex_array) {
	
	vertex_array.push_back(glm::vec4(x - w / 2, -y - h / 2, z, 0.0));
	vertex_array.push_back(glm::vec4(x - w / 2, -y + h / 2, z, 0.0));
	vertex_array.push_back(glm::vec4(x + w / 2, -y + h / 2, z, 0.0));
	vertex_array.push_back(glm::vec4(x + w / 2, -y + h / 2, z, 0.0));
	vertex_array.push_back(glm::vec4(x + w / 2, -y - h / 2, z, 0.0));
	vertex_array.push_back(glm::vec4(x - w / 2, -y - h / 2, z, 0.0));

}

void oglRender::addColor(glm::vec4 color, std::vector<glm::vec4> &color_array) {
		
		for (int i = 0; i < 6; i++) {
			color_array.push_back(color);
		}

}


void oglRender::addTex(char tex, std::vector<glm::vec3> &tex_array) {

		float x_start = (float)((int)(tex) % 16) / 16.0f;
		float y_start = 1.0f - (float)(((int)(tex) / 16) + 1.0) / 16.0f;
		float x_end = x_start + (1.0f / 16.0f);
		float y_end = (y_start + 1.0f / 16.0f);

		float s[6] = { x_start, x_start ,x_end,x_end,x_end,x_start };
		float t[6] = { y_start, y_end ,y_end,y_end,y_start,y_start };

		GLfloat type = tex != 0 ? 1.0 : 0.0;
		std::string valid = "@pSBMszCGhcP|)}[]({\\=\"_~?!$/,-%&";
		if (valid.find(tex) != std::string::npos) {
			type = 2.0;
		}

		for (int i = 0; i < 6; i++) {
			tex_array.push_back(glm::vec3(s[i], t[i], tex!=0?type:0.0));
		}

}

void oglRender::editColor(int x, int y, glm::vec4 color, std::vector<glm::vec4> &color_array, GLuint colorID) {
	int index = y*(MAX_H*6) + x*6;
	glBindBuffer(GL_ARRAY_BUFFER, colorID);
	std::vector<glm::vec4> c;
	for (int i = 0; i < 6; i++) {
		color_array[index + i] = color;
		c.push_back(color);
	}

	glBufferSubData(GL_ARRAY_BUFFER, (y*(MAX_H * 6) + x * 6)*4* sizeof(GLfloat), 6 * 4 * sizeof(GLfloat), &c.front());
}
void oglRender::editTex(int x, int y, char tex, std::vector<glm::vec3> &tex_array, GLuint texID, bool _seen) {
	int index = y*(MAX_H * 6) + x * 6;
	glBindBuffer(GL_ARRAY_BUFFER, texID);
	std::vector<glm::vec3> temp;

	float x_start = (float)((int)(tex) % 16) / 16.0f;
	float y_start = 1.0f - (float)(((int)(tex) / 16) + 1.0) / 16.0f;
	float x_end = x_start + (1.0f / 16.0f);
	float y_end = (y_start + 1.0f / 16.0f);


	float s[6] = { x_start, x_start ,x_end,x_end,x_end,x_start };
	float t[6] = { y_end, y_start ,y_start,y_start,y_end,y_end };


	for (int i = 0; i < 6; i++) {
		GLfloat type = tex != 0 ? 1.0 : 0.0;
		std::string valid = ".#<>";
		if ((tex>0 && tex<10) || (valid.find(tex) != std::string::npos) && !_seen) {
			type = 2.0;
		}

		glm::vec3 texture = glm::vec3(s[i], t[i],type);
		
		tex_array[index + i] = texture;
		temp.push_back(texture);
	}

	glBufferSubData(GL_ARRAY_BUFFER, (y*(MAX_H * 6) + x * 6) * 3 * sizeof(GLfloat), 6 * 3 * sizeof(GLfloat), &temp.front());
}
void oglRender::editBuffer(int index, float w, float h, glm::vec4 position, std::vector<glm::vec4> &position_array, GLuint bufferID) {
	glBindBuffer(GL_ARRAY_BUFFER, bufferID);
	float dx[6] = {-w/2, -w/2, w/2, w/2, w/2, -w/2};
	float dy[6] = {-h/2,  h/2, h/2, h/2,-h/2, -h/2};

	std::vector<glm::vec4> modify;
	for (int i = 0; i < 6; i++) {
		position_array[index + i] = position + glm::vec4(dx[i], dy[i], 0.0, 0.0);
		modify.push_back(position_array[index + i]);
	}
	glBufferSubData(GL_ARRAY_BUFFER, (index * 6) * 4 * sizeof(GLfloat), 6 * 4 * sizeof(GLfloat), &modify.front());
}


void oglRender::initOGL() {

	
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	

	
	std::vector<glm::vec4> g_dungeon_buffer_data;
	
	GLfloat i;
	GLfloat j;

	for (i = 0; i < MAX_V; i++) {
		for (j = 0; j < (MAX_H * 3 * 3 * 2); j += 3 * 3 * 2) {
			float x = j / (3 * 3 * 2);
			drawRectangle(x, i, 0.0, 1.0, 1.0, g_dungeon_buffer_data);
			char sym = d->checkMap(position_t((int)x, (int)i));
				addColor(glm::vec4(0.0, 1.0, 0.0, 1.0), g_dungeon_front_color_buffer_data);
				addColor(glm::vec4(0.0, 0.0, 0.0, 1.0), g_dungeon_back_color_buffer_data);
				addTex(sym, g_dungeon_tex_buffer_data);
		}
	
	}

	std::vector<character_t*> characterList;
	std::vector<character_t*>::iterator itr;
	
	characterList.push_back(p);

	for (itr = mList->begin(); itr != mList->end(); itr++) {
		characterList.push_back((*itr));
	}
	

	for (itr = characterList.begin(); itr != characterList.end(); itr++) {
		position_t pos = (*itr)->getPos();
		drawRectangle((GLfloat)pos.x(), (GLfloat)pos.y(), 0.5, 1.0, 1.0, g_character_buffer_data);
		addColor(glm::vec4(1.0, 0.0, 0.0, 1.0), g_character_frontcolor_buffer_data);
		addColor(glm::vec4(1.0, 0.0, 0.0, 1.0), g_character_backcolor_buffer_data);
		addTex((*itr)->getSym(), g_character_tex_buffer_data);
	}

	
	glGenBuffers(1, &dungeonbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, dungeonbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_dungeon_buffer_data.size()*4*sizeof(GLfloat), &g_dungeon_buffer_data.front(), GL_DYNAMIC_DRAW);

	glGenBuffers(1, &dungeonfrontcolorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, dungeonfrontcolorbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_dungeon_front_color_buffer_data.size() *4* sizeof(GLfloat), &g_dungeon_front_color_buffer_data.front(), GL_DYNAMIC_DRAW);

	glGenBuffers(1, &dungeonbackcolorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, dungeonbackcolorbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_dungeon_back_color_buffer_data.size() * 4 * sizeof(GLfloat), &g_dungeon_back_color_buffer_data.front(), GL_DYNAMIC_DRAW);


	glGenBuffers(1, &dungeontexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, dungeontexbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_dungeon_tex_buffer_data.size() * 3 * sizeof(GLfloat), &g_dungeon_tex_buffer_data.front(), GL_DYNAMIC_DRAW);

	glGenBuffers(1, &characterbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, characterbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_buffer_data.size() * 4 * sizeof(GLfloat), &g_character_buffer_data.front(), GL_DYNAMIC_DRAW);

	glGenBuffers(1, &characterfrontcolorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, characterfrontcolorbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_frontcolor_buffer_data.size() * 4 * sizeof(GLfloat), &g_character_frontcolor_buffer_data.front(), GL_DYNAMIC_DRAW);

	glGenBuffers(1, &characterbackcolorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, characterbackcolorbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_backcolor_buffer_data.size() * 4 * sizeof(GLfloat), &g_character_backcolor_buffer_data.front(), GL_DYNAMIC_DRAW);


	glGenBuffers(1, &charactertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, charactertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_tex_buffer_data.size() * 3 * sizeof(GLfloat), &g_character_tex_buffer_data.front(), GL_DYNAMIC_DRAW);


	std::string frag_name("./shaders/dungeon.frag");
	std::string vert_name("./shaders/dungeon.vert");
	dungeonPID = loadShaders(frag_name, vert_name);
	loadImage("./data/text.bmp");
	initDebugWindow();

}

void oglRender::initDebugWindow() {

	if (FT_Init_FreeType(&ft)) {
		std::cout << "Could not init freetype library" << std::endl;
	}

	if (FT_New_Face(ft, "./data/Arial.ttf", 0, &face)) {
		std::cout << "Could not open font" << std::endl;
	}

	FT_Set_Pixel_Sizes(face,0, 32);
	if (FT_Load_Char(face, 'X', FT_LOAD_RENDER)) {
		std::cout << "Could not load character 'X'\n" << std::endl;

	}

	g = face->glyph;


	std::string frag_name("./shaders/debug.frag");
	std::string vert_name("./shaders/debug.vert");
	debugPID = loadShaders(frag_name, vert_name);
	

	
	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &debugtex);
	GLint text = glGetUniformLocation(debugPID, "DebugText");
	glUniform1i(text, 1);
	glBindTexture(GL_TEXTURE_2D, debugtex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glGenBuffers(1, &debugbuffer);

}

GLFWwindow * oglRender::getWindow() {
	return window;
}
GLuint oglRender::loadShaders(std::string frag_name, std::string vert_name) {
	
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);



	std::cout << "Loading " << frag_name << std::endl;
	std::string frag_src = loadfile(frag_name);
	std::cout << "Loading " << vert_name << std::endl;
	std::string vert_src = loadfile(vert_name);

	const char* v_src = vert_src.c_str();
	const char* f_src = frag_src.c_str();
	glShaderSource(VertexShaderID, 1, &v_src, NULL);
	glShaderSource(FragmentShaderID, 1, &f_src, NULL);
	
	GLint Result = GL_FALSE;
	int InfoLogLength;

	glCompileShader(VertexShaderID);
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength > 0) {
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}


	glCompileShader(FragmentShaderID);
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);
	return ProgramID;
}

std::string oglRender::loadfile(const std::string &filename) {
	std::ifstream file(filename.c_str(), std::ios::binary);
	if (!file.good()){
		std::cout << "Failed to load: "<< filename << std::endl;
	}
	std::string str((std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());
	return str;
}
void oglRender::setRender(bool render) {
	_render = render;
}

void oglRender::swapBytes(int begin, int end, std::string &d) {
	
	for (int j = begin; j<end; j += 4) {
		char temp = d[j];
		for (int i = 0; i <3; i++) {
			d[j + i] = d[j + i + 1];
		}
		d[j + 3] = temp;
	}
}


GLuint oglRender::loadImage(std::string _fn) {
	std::string img_src = loadfile(_fn);
	std::cout << "Loading File: " << _fn << std::endl;
	std::string header = img_src.substr(0, img_src[14]);
	int data_position = (unsigned char)header[0x0a];
	int width = 0;
	int i;

	for (i = 0; i < 4; i++) {
		width += header[18+i]<<(8*i);
	}
	
	int height=0;
	
	for (i = 0; i < 4; i++) {
		height += header[22+i] << (8 * i);
	}
	
	int imageSize = 0;
	
	for (i = 0; i < 4; i++) {
		imageSize += header[34 + i] << (8 * i);
	}

	std::string data = img_src.substr(data_position, imageSize);

	
	std::string::iterator s_itr;
	std::vector<std::thread*> computeStack;
	int stride = height/8;
	for (int j = 0; j<height; j+=stride) {
		//swapBytes(j*width * 4, (j + stride)*width * 4, data);
		computeStack.push_back(new std::thread(&oglRender::swapBytes, this, j*width * 4, (j + stride)*width * 4, std::ref(data)));
	}
	std::vector<std::thread*>::iterator t_itr;
	for (t_itr = computeStack.begin(); t_itr != computeStack.end(); t_itr++) {
		(*t_itr)->join();
	}
	for (t_itr = computeStack.begin(); t_itr != computeStack.end(); t_itr++) {
		delete (*t_itr);
	}

	std::cout << data_position << '\t' << data.size() << '\t' << width << '\t' << height << '\t' << std::endl;
	
	
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &textureID);
	GLint text = glGetUniformLocation(dungeonPID, "TextImage");
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	glUniform1i(text, 0);
	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, &data.front());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	

	return textureID;
}

void oglRender::renderDungeon() {
	glm::mat4 Projection;

	glm::mat4 View = glm::lookAt(
		glm::vec3(MAX_H / 2 - 0.5, -MAX_V / 2, 1.0), 
		glm::vec3(MAX_H / 2 - 0.5, -MAX_V / 2, 0),
		glm::vec3(0, 1, 0)  
	);

	int height;
	glfwGetWindowSize(window, &dungeon_width, &height);
	float ratio = (float)dungeon_width / (float)height;
	dungeon_height = dungeon_width*MAX_V / MAX_H;
	dungeon_start = height /4;
	glViewport(0, dungeon_start, dungeon_width, dungeon_height);
	
	Projection = glm::ortho((float)(-MAX_H / 2), (float)(MAX_H / 2), (float)(-MAX_V / 2), (float)(MAX_V / 2), -1.0f, 1.0f);
	
	int i;
	int j;
	for (i = 0; i < MAX_V; i++) {
		for (j = 0; j < MAX_H; j++) {
			position_t pos = position_t(j, i);
			position_t delta = pos - p->getPos();
			char sym = d->checkMap(pos);
			float d2 = delta*delta;
			d2 = d2 == 0 ? 1.0 : d2;
			float s = p->_sight;
			if (!_revealed) {
				sym = p->seenMap[i][j];
			}

			if (sym !=0) {
					
					int time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
					if ( ((sym>0 && sym<10) || sym == '.' || sym == '#' ||sym =='<' || sym == '>') && ((p->checkLos(position_t(j,i)) && delta*delta <= p->_sight))) {
						editColor(j, i, glm::vec4(1.0, 0.90+0.10*std::sin((float)time/500.0), 0.5 + 0.5*std::sin((float)time / 500.0), 1.0-0.5*d2/s), g_dungeon_front_color_buffer_data, dungeonfrontcolorbuffer);
						editColor(j, i, glm::vec4(0.0, 0.0, 0.0, 0.0), g_dungeon_back_color_buffer_data, dungeonbackcolorbuffer);
						editTex(j, i, sym, g_dungeon_tex_buffer_data, dungeontexbuffer, false);
					}
					else {
						if (_revealed) {
							editColor(j, i, glm::vec4(1.0, 1.0, 1.0, 1.0), g_dungeon_front_color_buffer_data, dungeonfrontcolorbuffer);
							editColor(j, i, glm::vec4(0.0, 0.0, 0.0, 0.0), g_dungeon_back_color_buffer_data, dungeonbackcolorbuffer);
							editTex(j, i, sym, g_dungeon_tex_buffer_data, dungeontexbuffer, false);
						}
						else {
							editColor(j, i, glm::vec4(0.5, 0.5, 0.5, 0.5), g_dungeon_front_color_buffer_data, dungeonfrontcolorbuffer);
							editColor(j, i, glm::vec4(0.0, 0.0, 0.0, 1.0), g_dungeon_back_color_buffer_data, dungeonbackcolorbuffer);
							editTex(j, i, sym, g_dungeon_tex_buffer_data, dungeontexbuffer, false);
						}
					}
					

			}
			else {
				editColor(j, i, glm::vec4(0.0, 0.0, 0.0, 0.0), g_dungeon_front_color_buffer_data, dungeonfrontcolorbuffer);
				editColor(j, i, glm::vec4(0.0, 0.0, 0.0, 1.0), g_dungeon_back_color_buffer_data, dungeonbackcolorbuffer);
			}
		}

	}
	

	GLuint MatrixID = glGetUniformLocation(dungeonPID, "MVP");

	glUseProgram(dungeonPID);

	glm::mat4 Model = glm::mat4(1.0f);
	glm::mat4 mvp = Projection * View * Model;
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);


	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, dungeonbuffer);
	glVertexAttribPointer(0,
		4,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, dungeonfrontcolorbuffer);
	glVertexAttribPointer(
		1,                               
		4,                               
		GL_FLOAT,                        
		GL_FALSE,                         
		0,                                
		(void*)0                          
	);
	
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, dungeonbackcolorbuffer);
	glVertexAttribPointer(
		2,                                
		4,                                
		GL_FLOAT,                         
		GL_FALSE,                         
		0,                                
		(void*)0                          
	);
	
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, dungeontexbuffer);
	glVertexAttribPointer(
		3,                                
		3,                               
		GL_FLOAT,                         
		GL_FALSE,                        
		0,                                
		(void*)0                          
	);



	glDrawArrays(GL_TRIANGLES, 0, 3 * 2 * MAX_H*MAX_V);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
}


void oglRender::renderCharacters() {
	//glDisable(GL_BLEND);
	std::vector<character_t*> characterList;
	std::vector<character_t*>::iterator itr;

	characterList.push_back(p);

	for (itr = mList->begin(); itr != mList->end(); itr++) {
		if (itr == mList->end()) break;
		position_t deltaPos = (*itr)->getPos() - p->getPos();
		if((p->checkLos((*itr)->getPos()) && deltaPos*deltaPos <= p->_sight) || _revealed)
		characterList.push_back((*itr));
	}



	g_character_buffer_data.clear();
	g_character_frontcolor_buffer_data.clear();
	g_character_backcolor_buffer_data.clear();
	g_character_tex_buffer_data.clear();

	
	std::map<std::pair<int, int>, std::vector<object>>::iterator o_itr;
	for (o_itr = object_map->begin(); o_itr != object_map->end(); o_itr++) {
		if (o_itr == object_map->end()) break;
		if (object_map->size() < 1) break;
		position_t pos = (*o_itr).second.front().getPos();
		position_t deltaPos = pos - p->getPos();
		
		if ((p->checkLos(pos) && deltaPos*deltaPos <= p->_sight) || _revealed) {
			drawRectangle((GLfloat)pos.x(), (GLfloat)pos.y(), 0.25, 1.0, 1.0, g_character_buffer_data);

			int c_index = COLOR_WHITE;
			if ((*o_itr).second.front()._colorIndex.size() > 0)
				c_index = (*o_itr).second.front()._colorIndex.front();

			addColor(basic_color[c_index], g_character_frontcolor_buffer_data);
			if (c_index == COLOR_BLACK) {
				addColor(basic_color[COLOR_WHITE], g_character_backcolor_buffer_data);
			}
			else {
				addColor(basic_color[COLOR_BLACK], g_character_backcolor_buffer_data);
			}
			if ((*o_itr).second.size() > 1) {
				addTex('&', g_character_tex_buffer_data);
			}
			else {
				addTex((*o_itr).second.front()._sym, g_character_tex_buffer_data);
			}
		}
	}


	for (itr = characterList.begin(); itr != characterList.end(); itr++) {
		position_t pos = (*itr)->getPos();
		drawRectangle((GLfloat)pos.x(), (GLfloat)pos.y(), 0.5, 1.0, 1.0, g_character_buffer_data);
		int c_index = (*itr)->currentColor();
		glm::vec4 fColor = 0.7f*basic_color[c_index];
		fColor[3] = 1.0;
		if(c_index == COLOR_WHITE){
			fColor = basic_color[c_index];
		}

		
		addColor(fColor,g_character_frontcolor_buffer_data);
		if (c_index == COLOR_BLACK) {
			addColor(basic_color[COLOR_WHITE], g_character_backcolor_buffer_data);
		}
		else {
			addColor(basic_color[COLOR_BLACK], g_character_backcolor_buffer_data);
		}
		addTex((*itr)->getSym(), g_character_tex_buffer_data);
	}

	if (cursor_mode) {
		drawRectangle((GLfloat)cursor.x(), (GLfloat)cursor.y(), 0.25, 1.0, 1.0, g_character_buffer_data);
		if (getElapsedTime()/500 % 2) {
			addColor(basic_color[COLOR_WHITE], g_character_frontcolor_buffer_data);
			addColor(basic_color[COLOR_BLACK], g_character_backcolor_buffer_data);
		}
		else {
			addColor(basic_color[COLOR_BLACK], g_character_frontcolor_buffer_data);
			addColor(basic_color[COLOR_WHITE], g_character_backcolor_buffer_data);
		}
		
		addTex('*', g_character_tex_buffer_data);
	}
	
	glBindBuffer(GL_ARRAY_BUFFER, characterbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_buffer_data.size() * 4 * sizeof(GLfloat), &g_character_buffer_data.front(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, characterfrontcolorbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_frontcolor_buffer_data.size() * 4 * sizeof(GLfloat), &g_character_frontcolor_buffer_data.front(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, characterbackcolorbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_frontcolor_buffer_data.size() * 4 * sizeof(GLfloat), &g_character_backcolor_buffer_data.front(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, charactertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_character_tex_buffer_data.size() * 3 * sizeof(GLfloat), &g_character_tex_buffer_data.front(), GL_DYNAMIC_DRAW);


	glm::mat4 Projection;

	glm::mat4 View = glm::lookAt(
		glm::vec3(MAX_H / 2 - 0.5, -MAX_V / 2, 1.0), // Camera is at (4,3,3), in World Space
		glm::vec3(MAX_H / 2 - 0.5, -MAX_V / 2, 0), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);


	float ratio = (float)dungeon_width / (float)dungeon_height;
	glViewport(0, dungeon_start, dungeon_width, dungeon_height);
	Projection = glm::ortho((float)(-MAX_H / 2), (float)(MAX_H / 2), (float)(-MAX_V / 2), (float)(MAX_V / 2), -1.0f, 1.0f);

	GLuint MatrixID = glGetUniformLocation(dungeonPID, "MVP");

	glUseProgram(dungeonPID);

	glm::mat4 Model = glm::mat4(1.0f);
	glm::mat4 mvp = Projection * View * Model;
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);


	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, characterbuffer);
	glVertexAttribPointer(0,
		4,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, characterfrontcolorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		4,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, characterbackcolorbuffer);
	glVertexAttribPointer(
		2,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		4,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, charactertexbuffer);
	glVertexAttribPointer(
		3,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	glDrawArrays(GL_TRIANGLES, 0, g_character_buffer_data.size());
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	//glEnable(GL_BLEND);
}

void oglRender::setDebugMessage(std::string msg) {
	//debugMessage = msg;
	dMsg m;
	m.timestamp = std::chrono::system_clock::now();
	m.message = msg;
	dMsgList.push_back(m);

}


void oglRender::renderDebugWindow() {
	int width, height;
	glfwGetWindowSize(window, &width, &height);
	float debug_window_height = height - dungeon_start - dungeon_height;
	glViewport(0, dungeon_start+dungeon_height, dungeon_width, debug_window_height);

	glUseProgram(debugPID);

	GLuint fColor = glGetUniformLocation(debugPID, "FrontColor");

	glm::vec4 frontColor(1.0, 1.0, 1.0, 1.0);
	glUniform4fv(fColor, 1, &frontColor[0]);
	
	GLuint bColor = glGetUniformLocation(debugPID, "BackColor");
	glm::vec4 BorderColor(1.0, 1.0, 1.0, 0.0);
	glUniform4fv(bColor, 1, &BorderColor[0]);
	
	float x = -1.0;
	float y =  1.0;
	float sx = (1.0f / dungeon_width) ;
	float sy = (1.0f / debug_window_height);
	y -= 32.0f*1.0f/ debug_window_height;
	
	for (int j = dMsgList.size()-1; j > 0; j--) {
		if (stopWatch(dMsgList[j].timestamp) > 1000) {
			dMsgList.erase(dMsgList.begin() + j);
		}
	}
	for (int j = 0; j<dMsgList.size(); j++) {
		debugMessage = (dMsgList[j]).message;
		
		for (unsigned int i = 0; i < debugMessage.length(); i++) {
			if (debugMessage.length() < i) break;
			if (FT_Load_Char(face, debugMessage[i], FT_LOAD_RENDER))
				continue;

			glEnableVertexAttribArray(0);

			float x2 = x + g->bitmap_left * sx;
			float y2 = -y - g->bitmap_top * sy;
			float w = g->bitmap.width * sx;
			float h = g->bitmap.rows * sy;

			std::vector<glm::vec4> debugWindow;
			debugWindow.push_back(glm::vec4(x2, -y2, 0, 0));
			debugWindow.push_back(glm::vec4(x2 + w, -y2, 1, 0));
			debugWindow.push_back(glm::vec4(x2, -y2 - h, 0, 1));
			debugWindow.push_back(glm::vec4(x2 + w, -y2 - h, 1, 1));

			glBindBuffer(GL_ARRAY_BUFFER, debugbuffer);
			glBufferData(GL_ARRAY_BUFFER, debugWindow.size() * 4 * sizeof(GLfloat), &debugWindow.front(), GL_DYNAMIC_DRAW);



			glVertexAttribPointer(0,
				4,
				GL_FLOAT,
				GL_FALSE,
				0,
				(void*)0
			);

			GLint text = glGetUniformLocation(debugPID, "DebugText");

			glActiveTexture(GL_TEXTURE1);
			glUniform1i(text, 1);
			glBindTexture(GL_TEXTURE_2D, debugtex);

			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_RED,
				g->bitmap.width,
				g->bitmap.rows,
				0,
				GL_RED,
				GL_UNSIGNED_BYTE,
				g->bitmap.buffer
			);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glDisableVertexAttribArray(0);
			x += (g->advance.x / 64) * sx;
			y += (g->advance.y / 64) * sy;
		}
		x = -1.0;
		y -= 32.0f*1.0f / debug_window_height;
	}
}


void oglRender::renderList() {


	if (displayInventory || displayMonsterList || displayEquipment) {
		glm::mat4 Projection;

		glm::mat4 View = glm::lookAt(
			glm::vec3(MAX_H / 2 - 0.5, -MAX_V / 2, 1.0), // Camera is at (4,3,3), in World Space
			glm::vec3(MAX_H / 2 - 0.5, -MAX_V / 2, 0), // and looks at the origin
			glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
		);

		int width, height;
		glfwGetWindowSize(window, &width, &height);
		float ratio = (float)width / (float)height;
		glViewport(0, 0, width, height);

		glUseProgram(debugPID);

		GLuint fColor = glGetUniformLocation(debugPID, "FrontColor");

		glm::vec4 frontColor(1.0, 1.0, 1.0, 1.0);
		glUniform4fv(fColor, 1, &frontColor[0]);

		GLuint bColor = glGetUniformLocation(debugPID, "BackColor");
		glm::vec4 BorderColor(1.0, 0.0, 0.0, 1.0);
		glUniform4fv(bColor, 1, &BorderColor[0]);


		float x = -0.5;
		float y = 0.5;
		float sx = (1.0f / 1920.0f);
		float sy = (1.0f / 1080.0f);

	


		//Generate List
		std::vector<std::string> list;
		std::vector<std::string> descriptionList;
		std::string equipmentTypes[12] = { "WEAPON","OFFHAND","RANGED","ARMOR","HELMET","CLOAK","GLOVES","BOOTS", "RING1","RING2", "AMULET", "LIGHT" };
			if (displayInventory) {
				std::vector<object>::iterator i_itr;
				list.push_back("Inventory");
				for (i_itr = p->inventory.begin(); i_itr != p->inventory.end(); i_itr++) {
					list.push_back(std::to_string(i_itr - p->inventory.begin()) + " " + i_itr->_name);
				}
				for (int i = list.size(); i < 11; i++) {
					list.push_back(std::to_string(i - 1) + " " + "(EMPTY)");
				}
			}
			else if (displayMonsterList) {
				std::vector<character_t*>::iterator m_itr;
				list.push_back("Monster");
				for (m_itr = mList->begin(); m_itr != mList->end(); m_itr++) {
					list.push_back(((*m_itr)->_name));
				}
			}
			else if (displayEquipment) {

				
				std::string label("abcdefghijkl");
				//wclear(equipList);
				std::map<std::string, object>::iterator itr;
				list.push_back("Label Type\tEquipment");
				for (int i = 0; i<12; i++) {
					itr = p->equipment.find(equipmentTypes[i]);

					if (itr != p->equipment.end()) {
						std::stringstream stream;
						stream << equipmentTypes[i]<< '\t' << itr->second._name.c_str();
						list.push_back(stream.str());
					}
					else {
					
						list.push_back(equipmentTypes[i] + "\t" + "Empty");
					}
				}
			}
			//Draw border

			BorderColor = glm::vec4(0.0, 0.0, 0.0, 1.0);
			glUniform4fv(bColor, 1, &BorderColor[0]);

			glEnableVertexAttribArray(0);
			GLfloat yl = 13 * 32 * 1.0f / 1080.0f;
			GLfloat ys = y - 8 * 1.0f / 1080.0f;
			GLfloat xl = 21 * 32 * 1.0f / 1920.0f;
			GLfloat xs = x - 8 * 1.0f / 1920.0f;
		
			std::vector<glm::vec4> debugWindow;
			debugWindow.push_back(glm::vec4(xs, ys, 0, 0));
			debugWindow.push_back(glm::vec4(xs + xl, ys, 1, 0));
			debugWindow.push_back(glm::vec4(xs, ys - yl, 0, 1));
			debugWindow.push_back(glm::vec4(xs + xl, ys - yl, 1, 1));

			glBindBuffer(GL_ARRAY_BUFFER, debugbuffer);
			glBufferData(GL_ARRAY_BUFFER, debugWindow.size() * 4 * sizeof(GLfloat), &debugWindow.front(), GL_DYNAMIC_DRAW);



			glVertexAttribPointer(0,
				4,
				GL_FLOAT,
				GL_FALSE,
				0,
				(void*)0
			);

			GLint text = glGetUniformLocation(debugPID, "DebugText");

			glActiveTexture(GL_TEXTURE1);
			glUniform1i(text, 1);
			glBindTexture(GL_TEXTURE_2D, debugtex);
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_RED,
				g->bitmap.width,
				g->bitmap.rows,
				0,
				GL_RED,
				GL_UNSIGNED_BYTE,
				g->bitmap.buffer
			);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glDisableVertexAttribArray(0);

			//DrawSelection Box
			if (list.size() > 1) {
				BorderColor = glm::vec4(1.0, 1.0, 1.0, 1.0);
				glUniform4fv(bColor, 1, &BorderColor[0]);

				glEnableVertexAttribArray(0);


				yl = 32 * 1.0f / 1080.0f;
				ys = (float)(y - (itemSelection+1) * 32 * 1.0 / 1080.0f - 8 * 1.0f / 1080.0f);
				debugWindow.clear();
				debugWindow.push_back(glm::vec4(xs, ys, 0, 0));
				debugWindow.push_back(glm::vec4(xs + xl, ys, 1, 0));
				debugWindow.push_back(glm::vec4(xs, ys - yl, 0, 1));
				debugWindow.push_back(glm::vec4(xs + xl, ys - yl, 1, 1));
				glBindBuffer(GL_ARRAY_BUFFER, debugbuffer);
				glBufferData(GL_ARRAY_BUFFER, debugWindow.size() * 4 * sizeof(GLfloat), &debugWindow.front(), GL_DYNAMIC_DRAW);



				glVertexAttribPointer(0,
					4,
					GL_FLOAT,
					GL_FALSE,
					0,
					(void*)0
				);

				text = glGetUniformLocation(debugPID, "DebugText");
				glActiveTexture(GL_TEXTURE1);
				glUniform1i(text, 1);
				glBindTexture(GL_TEXTURE_2D, debugtex);
				glTexImage2D(
					GL_TEXTURE_2D,
					0,
					GL_RED,
					g->bitmap.width,
					g->bitmap.rows,
					0,
					GL_RED,
					GL_UNSIGNED_BYTE,
					g->bitmap.buffer
				);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				glDisableVertexAttribArray(0);
			}

	

		BorderColor = glm::vec4(1.0, 1.0, 1.0, 0.0);
		glUniform4fv(bColor, 1, &BorderColor[0]);

		std::vector<std::string>::iterator itr;
	
		y -= 32 * 1.0f / 1080.0f;
		unsigned int count = 0;
		for (itr = list.begin(); itr != list.end(); itr++) {
			count = 0;
			for (unsigned int i = 0; i < (*itr).length(); i++) {
				count++;
				if ((*itr)[i] == '\t') {
					unsigned int column = count / 12 + 1;
					x = -0.5 + column * 6 * 32 * 1.0f / 1920.0f;
					continue;
				}
				if (FT_Load_Char(face,(*itr)[i], FT_LOAD_RENDER))
					continue;




				glEnableVertexAttribArray(0);
				if ((itr - list.begin()-1 == itemSelection)) {
					frontColor = glm::vec4(0.0, 0.0, 0.0, 1.0);
				}
				else {
					frontColor = glm::vec4(1.0, 1.0, 1.0, 1.0);
					if (displayMonsterList) {
						int index = (itr - list.begin() - 1);
						if (index >= 0) {
							int c_index = mList->at(index)->currentColor();
							c_index = c_index == COLOR_BLACK ? COLOR_WHITE : c_index;
							frontColor = basic_color[c_index];

						}
					}


				}



				glUniform4fv(fColor, 1, &frontColor[0]);
				BorderColor = glm::vec4(1.0, 1.0, 1.0, 0.0);
				glUniform4fv(bColor, 1, &BorderColor[0]);

				float x2 = x + g->bitmap_left * sx;
				float y2 = -y - g->bitmap_top * sy;
				float w = g->bitmap.width * sx;
				float h = g->bitmap.rows * sy;

				std::vector<glm::vec4> debugWindow;
				debugWindow.push_back(glm::vec4(x2, -y2, 0, 0));
				debugWindow.push_back(glm::vec4(x2 + w, -y2, 1, 0));
				debugWindow.push_back(glm::vec4(x2, -y2 - h, 0, 1));
				debugWindow.push_back(glm::vec4(x2 + w, -y2 - h, 1, 1));

				glBindBuffer(GL_ARRAY_BUFFER, debugbuffer);
				glBufferData(GL_ARRAY_BUFFER, debugWindow.size() * 4 * sizeof(GLfloat), &debugWindow.front(), GL_DYNAMIC_DRAW);



				glVertexAttribPointer(0,
					4,
					GL_FLOAT,
					GL_FALSE,
					0,
					(void*)0
				);

				GLint text = glGetUniformLocation(debugPID, "DebugText");

				glActiveTexture(GL_TEXTURE1);
				glUniform1i(text, 1);
				glBindTexture(GL_TEXTURE_2D, debugtex);

				glTexImage2D(
					GL_TEXTURE_2D,
					0,
					GL_RED,
					g->bitmap.width,
					g->bitmap.rows,
					0,
					GL_RED,
					GL_UNSIGNED_BYTE,
					g->bitmap.buffer
				);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
				glDisableVertexAttribArray(0);
				x += (g->advance.x / 64) * sx;
				y += (g->advance.y / 64) * sy;
			}
			x = -0.5;
			y -= 32 * 1.0f / 1080.0f;
		}


		

		//Draw  Item Description Border

		if ((displayInventory && (unsigned int)itemSelection < p->inventory.size()) || displayMonsterList) {
			BorderColor = glm::vec4(0.0, 0.0, 0.0, 1.0);
			glUniform4fv(bColor, 1, &BorderColor[0]);
			x = -0.5;
			y = 0.5;
			glEnableVertexAttribArray(0);
			GLfloat yl = 11 * 32 * 1.0f / 1080.0f;
			GLfloat ys = y - 8 * 1.0f / 1080.0f;
			GLfloat xl = 35 * 32 * 1.0f / 1920.0f;
			GLfloat xs = x + 21*32 * 1.0f / 1920.0f;
			x = xs;
			std::vector<glm::vec4> debugWindow;
			debugWindow.push_back(glm::vec4(xs, ys, 0, 0));
			debugWindow.push_back(glm::vec4(xs + xl, ys, 1, 0));
			debugWindow.push_back(glm::vec4(xs, ys - yl, 0, 1));
			debugWindow.push_back(glm::vec4(xs + xl, ys - yl, 1, 1));

			glBindBuffer(GL_ARRAY_BUFFER, debugbuffer);
			glBufferData(GL_ARRAY_BUFFER, debugWindow.size() * 4 * sizeof(GLfloat), &debugWindow.front(), GL_DYNAMIC_DRAW);



			glVertexAttribPointer(0,
				4,
				GL_FLOAT,
				GL_FALSE,
				0,
				(void*)0
			);

			GLint text = glGetUniformLocation(debugPID, "DebugText");

			glActiveTexture(GL_TEXTURE1);
			glUniform1i(text, 1);
			glBindTexture(GL_TEXTURE_2D, debugtex);
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_RED,
				g->bitmap.width,
				g->bitmap.rows,
				0,
				GL_RED,
				GL_UNSIGNED_BYTE,
				g->bitmap.buffer
			);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glDisableVertexAttribArray(0);
			if (displayInventory) {
				object printObject = p->inventory[itemSelection];
				descriptionList.push_back(index2type(printObject._type) + " ");
				descriptionList.push_back(printObject._name);
				descriptionList.push_back(std::string("Damage: ") + std::to_string(printObject._dam.min()) + "-" + std::to_string(printObject._dam.max()));
				descriptionList.push_back(std::string("Hit: ") + std::to_string(printObject._hit) + " " +
					std::string("Dodge: ") + std::to_string(printObject._dodge) + " " +
					std::string("Def: ") + std::to_string(printObject._def));

				descriptionList.push_back(std::string("Speed: ") + std::to_string(printObject._speed) + " " +
					std::string("Weight: ") + std::to_string(printObject._weight) + " " +
					std::string("Val: ") + std::to_string(printObject._val));
				descriptionList.push_back(std::string("Attr: ") + std::to_string(printObject._attr));
				descriptionList.push_back(printObject._desc);
			}
			else if(displayMonsterList) {
				character_t* printCharacter = mList->at(itemSelection);
				descriptionList.push_back(printCharacter->_name);
				descriptionList.push_back(std::string("Damage: ") + std::to_string(printCharacter->_dam.min()) + "-" + std::to_string(printCharacter->_dam.max()));
				descriptionList.push_back(std::string("Speed: ") + std::to_string(printCharacter->speed));
				descriptionList.push_back(std::string("Abilities: ") + index2ability(printCharacter->attr));
				descriptionList.push_back(printCharacter->_desc);
			}


			y -= 32 * 1.0f / 1080.0f;

			for (itr = descriptionList.begin(); itr != descriptionList.end(); itr++) {

				for (unsigned int i = 0; i < (*itr).length(); i++) {
					if ((*itr)[i] == '\n') {
						x = -0.5f + 21 * 32 * 1.0f / 1920.0f;
						y -= 32 * 1.0f / 1080.0f;
						continue;
					}
					if (FT_Load_Char(face, (*itr)[i], FT_LOAD_RENDER))
						continue;

					glEnableVertexAttribArray(0);
					frontColor = basic_color[COLOR_WHITE];

					if (displayInventory && (itr - (descriptionList.begin())) == 1) {
						
						frontColor = basic_color[COLOR_WHITE];
						if (p->inventory[itemSelection]._colorIndex.size() > 0) {
							int c_index = p->inventory[itemSelection]._colorIndex.front();
							c_index = c_index == COLOR_BLACK ? COLOR_WHITE : c_index;
							frontColor = basic_color[c_index];
						}
					}


					if (displayMonsterList && descriptionList.begin() == itr) {
						int c_index = mList->at(itemSelection)->currentColor();
						c_index = c_index == COLOR_BLACK ? COLOR_WHITE : c_index;
						frontColor = basic_color[c_index];
					}

					glUniform4fv(fColor, 1, &frontColor[0]);
					BorderColor = glm::vec4(1.0, 1.0, 1.0, 0.0);
					glUniform4fv(bColor, 1, &BorderColor[0]);

					float x2 = x + g->bitmap_left * sx;
					float y2 = -y - g->bitmap_top * sy;
					float w = g->bitmap.width * sx;
					float h = g->bitmap.rows * sy;

					std::vector<glm::vec4> debugWindow;
					debugWindow.push_back(glm::vec4(x2, -y2, 0, 0));
					debugWindow.push_back(glm::vec4(x2 + w, -y2, 1, 0));
					debugWindow.push_back(glm::vec4(x2, -y2 - h, 0, 1));
					debugWindow.push_back(glm::vec4(x2 + w, -y2 - h, 1, 1));

					glBindBuffer(GL_ARRAY_BUFFER, debugbuffer);
					glBufferData(GL_ARRAY_BUFFER, debugWindow.size() * 4 * sizeof(GLfloat), &debugWindow.front(), GL_DYNAMIC_DRAW);



					glVertexAttribPointer(0,
						4,
						GL_FLOAT,
						GL_FALSE,
						0,
						(void*)0
					);

					GLint text = glGetUniformLocation(debugPID, "DebugText");

					glActiveTexture(GL_TEXTURE1);
					glUniform1i(text, 1);
					glBindTexture(GL_TEXTURE_2D, debugtex);

					glTexImage2D(
						GL_TEXTURE_2D,
						0,
						GL_RED,
						g->bitmap.width,
						g->bitmap.rows,
						0,
						GL_RED,
						GL_UNSIGNED_BYTE,
						g->bitmap.buffer
					);

					glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
					glDisableVertexAttribArray(0);
					x += (g->advance.x / 64) * sx;
					y += (g->advance.y / 64) * sy;
				}
				
				
				if(displayInventory && (itr-(descriptionList.begin())) >= 1){
					x = -0.5f + 21 * 32 * 1.0f / 1920.0f;
					y -= 32 * 1.0f / 1080.0f;
				}

				if (displayMonsterList) {
					x = -0.5f + 21 * 32 * 1.0f / 1920.0f;
					y -= 32 * 1.0f / 1080.0f;
				}

				
			}


		}
	}
}

void oglRender::renderPlayerStatus() {
	

	
	glViewport(0, 0, dungeon_width, dungeon_start);
	float ratio = (float)dungeon_width / (float)dungeon_start;

	glUseProgram(debugPID);

	GLuint fColor = glGetUniformLocation(debugPID, "FrontColor");

	glm::vec4 frontColor(1.0, 1.0, 1.0, 1.0);
	glUniform4fv(fColor, 1, &frontColor[0]);

	GLuint bColor = glGetUniformLocation(debugPID, "BackColor");
	glm::vec4 BorderColor(1.0, 1.0, 1.0, 0.0);
	glUniform4fv(bColor, 1, &BorderColor[0]);

	float x = -1.0;
	float y = 1.0;
	float sx = (1.0f / dungeon_width);
	float sy = (1.0f / dungeon_start);
	y -= 32.0f*1.0f / dungeon_start;
	std::stringstream stats;
	
	stats << std::string("HP: ") + std::to_string(p->_hp) + "/" + std::to_string(p->_maxhp) << '\n';
	stats << std::string("Spd: ") + std::to_string(p->speed) << '\t'<<std::string("Def: ") + std::to_string(p->_def) << '\n';
	stats << std::string("Hit: ") + std::to_string(p->_hit) << '\t' << std::string("Dodge: ") + std::to_string(p->_dodge);
	
	std::string message = stats.str();

	int count = 0;
	for (unsigned int i = 0; i<message.length(); i++) {
		count++;
		if (message.length() < i) break;
		if (message[i] == '\n') {
			x = -1.0;
			y -= 32 * sy;
			count = 0;
			continue;
		}
		if (message[i] == '\t') {
		
			unsigned int column = count / 12 + 1;
			x = -1.0 + column * 6 * 32 * sx;
			count = column * 12;
			continue;
		}

		if (FT_Load_Char(face, message[i], FT_LOAD_RENDER))
			continue;

		glEnableVertexAttribArray(0);

		float x2 = x + g->bitmap_left * sx;
		float y2 = -y - g->bitmap_top * sy;
		float w = g->bitmap.width * sx;
		float h = g->bitmap.rows * sy;

		std::vector<glm::vec4> debugWindow;
		debugWindow.push_back(glm::vec4(x2, -y2, 0, 0));
		debugWindow.push_back(glm::vec4(x2 + w, -y2, 1, 0));
		debugWindow.push_back(glm::vec4(x2, -y2 - h, 0, 1));
		debugWindow.push_back(glm::vec4(x2 + w, -y2 - h, 1, 1));

		glBindBuffer(GL_ARRAY_BUFFER, debugbuffer);
		glBufferData(GL_ARRAY_BUFFER, debugWindow.size() * 4 * sizeof(GLfloat), &debugWindow.front(), GL_DYNAMIC_DRAW);



		glVertexAttribPointer(0,
			4,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);

		GLint text = glGetUniformLocation(debugPID, "DebugText");

		glActiveTexture(GL_TEXTURE1);
		glUniform1i(text, 1);
		glBindTexture(GL_TEXTURE_2D, debugtex);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			g->bitmap.width,
			g->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			g->bitmap.buffer
		);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glDisableVertexAttribArray(0);
		x += (g->advance.x / 64) * sx;
		y += (g->advance.y / 64) * sy;
	}

}

void oglRender::Render() {
	_render = false;
	initGLFW();
	initOGL();

	glm::mat4 Projection;

	glm::mat4 View = glm::lookAt(
		glm::vec3(MAX_H / 2-0.5, -MAX_V / 2, 1.0), // Camera is at (4,3,3), in World Space
		glm::vec3(MAX_H / 2-0.5, -MAX_V / 2, 0), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);


	glfwMakeContextCurrent(window);
	
	_render = true;

	startTime = std::chrono::system_clock::now();
	do {
		renderTime = std::chrono::high_resolution_clock::now();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.0, 0.0,0.0, 1.0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		renderDungeon();
		renderCharacters();
		renderDebugWindow();
		renderPlayerStatus();
		renderList();

		glfwPollEvents();
		glfwSwapBuffers(window);
	} while (_render);


	glDeleteBuffers(1, &VertexArrayID);
	glDeleteBuffers(1, &dungeonbuffer);
	glDeleteBuffers(1, &dungeonfrontcolorbuffer);
	glDeleteBuffers(1, &dungeonbackcolorbuffer);
	glDeleteBuffers(1, &dungeontexbuffer);
	glDeleteBuffers(1, &characterbuffer);
	glDeleteBuffers(1, &characterfrontcolorbuffer);
	glDeleteBuffers(1, &characterbackcolorbuffer);
	glDeleteBuffers(1, &charactertexbuffer);

	glDeleteProgram(dungeonPID);
	glDeleteProgram(debugPID);
}