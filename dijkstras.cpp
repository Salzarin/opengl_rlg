#include "heap.h"
#include "dijkstras.h"



class vertex{
public:
  int Vertex;
  int Weight;
  int Parent;
  int ID;
  
  vertex(int id, int v,int w,int p){
    ID = id;
    Vertex = v;
    Weight = w;
    Parent = p;
  }
  vertex(){
    ID = 0;
    Vertex = -1;
    Weight = INT_MAX;
    Parent = -1;
  }
    bool operator()(vertex* a,vertex* b,bool swap){
      
	if(swap){
	  int temp = (*b).ID;
	  (*b).ID = (*a).ID;
	  (*a).ID = temp;
	}
      return (*a).Weight<(*b).Weight;
    }
    
    int operator()(vertex* a){ 
      return (*a).ID;
    }
};


int edgeCheck(unsigned int map[MAX_V][MAX_H], int &index, int i, bool monster, bool tunneler){
  
  int cells[8];
  cells[0] = index-MAX_H;//Up
  cells[1] = index+1;//Right
  cells[2] = index+MAX_H;//Down
  cells[3] = index-1;//Left

  cells[4] = index-MAX_H-1;//Up-Left
  cells[5] = index-MAX_H+1;//Up-Right
  cells[6] = index+MAX_H+1;//Down-Right
  cells[7] = index+MAX_H-1;//Down-Left
  
  int check = cells[i];
  index = check;
  int x = cells[i]%MAX_H;
  int y = cells[i]/MAX_H;
  
  if(x>0 && x<MAX_H && y>0 && y<MAX_V){
    int hard = map[y][x];
    if(map[y][x] != 255){ //As Long as it's not a wall
      if(monster){ //If it is a monster
	if(tunneler){
	  if(hard+1 != 255){
	    hard = hard==0 ? 1 : 1+(hard)/85;
	    return hard;
	  }
	}
	else{
	  if(hard == 0){
	    return hard;
	  }					
	}
      }
      else{ //If it is cooridor
	return hard;
      }
    }
    return hard;
  }
  
  return INT_MAX;
  
}


void dijkstras(unsigned int map[MAX_V][MAX_H], int start, int end, bool monster, bool tunneler, int PMap[MAX_V][MAX_H]){


int i;
int total_vertex = MAX_H*MAX_V;
std::vector<int> path;

vertex min;
minHeap<vertex> heap(total_vertex);

//Initial all routes.
for(i = 0; i<total_vertex;i++){
	heap.push(vertex(i,i,INT_MAX,-1));
	PMap[i/MAX_H][i%MAX_H] = -1;
}

//Set Distance of Source to zero

(*(heap.list[start])).Weight = 0;
PMap[start/MAX_H][start%MAX_H] = start;
heap.buildHeap();
while(heap.heap.size()){

	min = heap.pop(); 
	//edge node= edgeList[min.Vertex];
	//edge::iterator etr;
	
	if(start!=end && min.Vertex == end){
	  break;
	}
	int j = monster?8:4;
	  //for(etr = node.begin(); etr!=node.end();etr++){
	for(i = 0;i<j;i++){
		  
		  int check = min.Vertex;
		  int w = edgeCheck(map, check,i,monster,tunneler); // This will modify check as well. 
		  int u = min.Weight;
		  int v = heap.peekHeap(check).Weight;
		  
		  if(u != INT_MAX && (v > (u+w)) && w!=INT_MAX){
			  
			  v = u+w;
			  PMap[check/MAX_H][check%MAX_H] = min.Vertex;
			  vertex change = heap.peekHeap(check);
			  change.Weight = v;
			  heap.decreaseKey(check,change);
		  }
	  
	  }
}

}