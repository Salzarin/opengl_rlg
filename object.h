#ifndef OBJECT_H
#define OBJECT_H
#include "util.h"
#include "dungeon.h"

class object{
public:
  object();
  virtual ~object();
  object(std::vector<std::string> arg);
  
  
  
  std::string _name;
  std::string _desc;
  std::vector<std::string> _descList;
  int _type;
  int _color;
  int _hit;
  dice _dam;
  int _dodge;
  int _def;
  int _weight;
  int _speed;
  int _attr;
  int _val;
  char _sym;
  dice _hitDice;
  //dice _dam;
  dice _dodgeDice;
  dice _defDice;
  dice _weightDice;
  dice _speedDice;
  dice _attrDice;
  dice _valDice;
  position_t _pos;
  
  std::vector<int> _colorIndex;
  int parseColor(std::string c);
  int parseAbil(std::string a);
  int parseHP(std::string h);
  dice parseDam(std::string d);
  int parseSpeed(std::string s);
  void parseObject();
  void setSym();
  position_t getPos();
  void setPos(position_t pos);
  void setGame(Dungeon * dungeon);
  void setRandomPos();
  void renderObject();
  int parseColor(int i);
  void reRoll();
  
  Dungeon* d;
  
  
};
#endif