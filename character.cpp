#include "character.h"


character_t::character_t(){
  setTime();
  _isAlive = true;
  
}

character_t::~character_t(){

  
}
bool character_t::decreaseHp(int i){
  _hp-=i;
  return _hp>0;
}

int character_t::damageRoll(){
  int total_damage = _dam.roll();
  std::map<std::string,object>::iterator itr;
  itr = equipment.find(std::string("WEAPON"));
  
  if(itr!=equipment.end()) total_damage = 0;
  
  for(itr = equipment.begin(); itr!=equipment.end();itr++){
    total_damage+=(itr->second._dam.roll());
  }
  
  return total_damage;
}

void character_t::reRoll(){
  _hp = _hpDice.roll();
  speed = _speedDice.roll();
}


int character_t::parseColor(std::string c){
    _color = color2index(c);
    for(int i = 0; i<8;i++){
      if(_color&(0x1<<i)){
	_colorIndex.push_back(i);
      }
    }
  return _color;
}

int character_t::currentColor() {
	int size = _colorIndex.size();
	if (size == 0) {
		return COLOR_WHITE;
	}
	else {
		return _colorIndex[(getTime() / 250) % size];
	}

}


int character_t::parseAbil(std::string a){
  return ability2index(a);
}

int character_t::parseSpeed(std::string s){
  _speedDice = dice(s);
  return _speedDice.roll();
}

int character_t::parseHP(std::string h){
  _hpDice = dice(h);
return _hpDice.roll();  
}
dice character_t::parseDam(std::string d){
 return dice(d); 
}

bool character_t::operator()(character_t* a,character_t* b){
	if((*a).eventTime==(*b).eventTime){
	   return (*a).getTime()<(*b).getTime();
	}
	return (*a).eventTime<(*b).eventTime;
}

void character_t::setSym(char c){
  sym = c;
}

char character_t::getSym(){
   return sym;
}

void character_t::setTime(){
  startTime = std::chrono::system_clock::now();
}

void character_t::setEventTime(){
  speed = speed == 0? 1:speed;
  eventTime = 1000/speed;
  setTime();
}

void character_t::setEventTime(int e){
  eventTime = e;
  setTime();
}



int character_t::getEventTime(){
  return eventTime;
}
int character_t::getSpeed(){
  return speed;
}

void character_t::decreaseEventTime(int i){
  eventTime-=i;
  eventTime = eventTime >0?eventTime:0;
}


unsigned int character_t::getTime(){
  return std::chrono::duration_cast<std::chrono::milliseconds>
  (std::chrono::system_clock::now() - startTime).count();
}

void character_t::setPos(position_t p){
  pos = p;
  npos = p;
}
position_t character_t::getPos(){
  return pos;
}
position_t character_t::getNPos(){
  return npos;
}


void character_t::setAlive(bool a){
_isAlive = a;
}

bool character_t::getAlive(){
return _isAlive;
}

void character_t::renderCharacter(){

}

bool character_t::addItem(object item){
  inventory.push_back(item);
  return false;
}

object character_t::dropItem(unsigned int i){
  object dropped;
  dropped._name = "Item Not Found.";
  dropped._type = 0;
  dropped.setSym();
  if(inventory.size()>i){
    dropped = inventory[i];
    dropped.setPos(pos);
    inventory.erase(inventory.begin()+i);
  }
  
  return dropped;
}

bool character_t::equipItem(int i){
    
    int ValidTypes = 0x7FF; 
    object equip = inventory[i];
    if(ValidTypes&equip._type){
      std::pair<std::map<std::string,object>::iterator,bool> itr;
      std::string type = index2type(equip._type);
      if(equip._type&(0x1<<8)){
	type = std::string("RING1");
      }
      itr = equipment.insert(std::pair<std::string,object>(type,equip));
      if(!itr.second){
	if(equip._type&(0x1<<8)){
	  type = std::string("RING2");
	  itr = equipment.insert(std::pair<std::string,object>(type,equip));
	  if(itr.second){
	    inventory.erase(inventory.begin()+i);
	    return true;
	  }
	}
	inventory[i] = itr.first->second;
	itr.first->second = equip;
      }
      else{
	inventory.erase(inventory.begin()+i);
      }
      return true;
    }
    
    return false;
}
  
  
object character_t::unequipItem(int type){  
    
    std::string equipmentTypes[12] = {"WEAPON","OFFHAND","RANGED","ARMOR","HELMET","CLOAK","GLOVES","BOOTS", "RING1","RING2", "AMULET", "LIGHT"};
    std::map<std::string,object>::iterator itr;
    
    itr = equipment.find(equipmentTypes[type]);
    
    object temp;
    //temp._sym = '*';
    temp._type = 0;
    if(itr!=equipment.end()){
      if(inventory.size() < 10 ){
      temp._name = "Object Inserted In Inventory";
		if(itr->second._type !=0){
		  inventory.push_back(itr->second);
		  temp = itr->second;
		  equipment.erase(itr);
		}
      }
      else{
		temp = itr->second;
		equipment.erase(itr);
      }
    }
    else{
      temp._name = "No Equipment to Remove.";
    }
    //temp.setSym();
    return temp;
}


