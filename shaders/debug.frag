#version 330 core

in vec2 texcoord;
in vec4 fcolor;
in vec4 bcolor;

out vec4 color;
uniform sampler2D DebugText;

void main(){


	//color =vec4(1.0,1.0,1.0,texture2D(DebugText,texcoord).r)*fcolor;
	color = (texture(DebugText,texcoord).r) * fcolor;	
	if(bcolor.a > 0.0){
		color = bcolor;
	}

}