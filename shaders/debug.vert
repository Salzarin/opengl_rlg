#version 330 core


layout(location = 0) in vec4 vertexPosition_modelspace;


uniform mat4 MVP;
uniform vec4 FrontColor;
uniform vec4 BackColor;

out vec2 texcoord;
out vec4 fcolor;
out vec4 bcolor;

void main(){	


	gl_Position =  vec4(vertexPosition_modelspace.xy,0.7,1);
	texcoord = vertexPosition_modelspace.zw;
	fcolor = FrontColor;
	bcolor = BackColor;
}