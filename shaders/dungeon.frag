#version 330 core

// Interpolated values from the vertex shaders
in vec4 frontColor;
in vec4 backColor;
in vec3 UV;
// Ouput data
out vec4 color;
uniform sampler2D TextImage;

void main(){

	// Output color = color specified in the vertex shader, 
	// interpolated between all 3 surrounding vertices
	
	if(UV.z > 1.5){
		color = texture(TextImage,UV.xy)*frontColor;
	}
	else if(UV.z>0.5){
		color = (texture(TextImage,UV.xy).a) * frontColor;	
	}
	else{		
		color = frontColor;
	}
}