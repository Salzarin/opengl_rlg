#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec4 vertexPosition_modelspace;
layout(location = 1) in vec4 frontvertexColor;
layout(location = 2) in vec4 backvertexColor;
layout(location = 3) in vec3 texcoord;
// Output data ; will be interpolated for each fragment.
out vec4 frontColor;
out vec4 backColor;
out vec3 UV;
// Values that stay constant for the whole mesh.
uniform mat4 MVP;

void main(){	

	// Output position of the vertex, in clip space : MVP * position
	gl_Position =  MVP * vec4(vertexPosition_modelspace.xyz,1);

	// The color of each vertex will be interpolated
	// to produce the color of each fragment
	frontColor = frontvertexColor;
	backColor = backvertexColor;
	UV = texcoord;
}