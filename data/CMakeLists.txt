cmake_minimum_required (VERSION 2.6)
file(GLOB DATA RELATIVE ${CMAKE_SOURCE_DIR}
		"*.txt"
		"*.bmp"
		"*.ttf"
		"*.rlg327"
		)
set(DATA ${DATA} PARENT_SCOPE)
