#include "monster.h"  
monster_t::monster_t(){

  
}
monster_t::~monster_t(){

  
}

monster_t::monster_t(std::string name,std::string desc,std::string symbol,std::string color, std::string s, std::string abil,std::string hp,std::string dam){
  _name = name;
  _desc = desc;
  setSym(symbol.c_str()[0]);
  //_color = 0;
  _color = parseColor(color);
  speed = parseSpeed(s);
  attr = parseAbil(abil);
  _hp = parseHP(hp);
  _dam = parseDam(dam);
  seen = false;
  _isAlive = true;
}



monster_t::monster_t(player_t* p, Dungeon * dungeon, int spd) : monster_t(){
  speed = spd;
  seen = false;
  _isAlive = true;
  d = dungeon;
  player = p;
}  


void monster_t::setGame(player_t* p,Dungeon * dungeon){
  player = p;
  d = dungeon;
}

bool monster_t::initMonsterPosition(bool mMap[MAX_V][MAX_H]){
  setRandomPos();
  int count=0;
  while((player->getPos()-pos)*(player->getPos()-pos) < 16 || mMap[pos.y()][pos.x()]){
    setRandomPos();
    count++;
    if(count > 1000){
      npos = pos;
      dest = pos;
      reRoll();
      return false;
    }
  }
  npos = pos;
  dest = pos;
  reRoll();
  setEventTime();
  //initMonsterAttr();
  return true;
}
void monster_t::initMonsterAttr(){
  char hex[17] = "0123456789abcdef";
  
  attr = 0;
  int j;
    for(j=0;j<4;j++){
      int randAttr = rand()%2;
      if(!randAttr){
	attr += 1<<j;
      }
    }
  setSym(hex[attr]);
  setEventTime();  
}


void monster_t::monsterNextMove(){

  bool intelligent = attr & 0x01;
  bool telepath = (attr>>1) & 0x01;
  bool tunneler = (attr>>2) & 0x01;
  bool erratic = (attr>>3) & 0x01; 
  checkLos();
  if(erratic){  
    if(rand()%2){
      //Erratic Movement
      npos = position_t(-1,-1);
      position_t dp = position_t(0,0);
      while((npos.x()<1 || npos.x()>(MAX_H-2) || npos.y()< 1 || npos.y() >(MAX_V-2))){
	dp = getnewDirection(rand()%8);
	npos = pos+dp;
      }
      return;
    }
  }
  //First Check if you know where you're going
  //Knows Player Location or Last know position
  if(telepath){
  //Intelligent uses map  
    if(intelligent){
      //Selects Which Map it uses
      
      if(tunneler){
	npos = d->getNext(pos,true);
      }
      else{
	npos = d->getNext(pos,false);
      }

    }
    else{
      //Go Straight Toward Player
      int dx = player->getPos().x() - pos.x();
      int dy = player->getPos().y() - pos.y();
      dx = dx < 0 ? -1: dx == 0 ? 0 : 1;
      dy = dy < 0 ? -1: dy == 0 ? 0 : 1;
      npos = pos + position_t(dx,dy);
    }
  }
  else{
    if(seen){
      int dx = player->getPos().x() - pos.x();
      int dy = player->getPos().y() - pos.y();
      dx = dx < 0 ? -1: dx == 0 ? 0 : 1;
      dy = dy < 0 ? -1: dy == 0 ? 0 : 1;
      npos = pos + position_t(dx,dy);
    }
    else{
      
      if(intelligent){ //Will Navigate to last seen location.
	int dx = dest.x() - pos.x();
	int dy = dest.y() - pos.y();
	dx = dx < 0 ? -1: dx == 0 ? 0 : 1;
	dy = dy < 0 ? -1: dy == 0 ? 0 : 1;
	npos = pos + position_t(dx,dy);
      }
      else{
      npos = pos; //Dumb Monsters that Don't see, Stays here.
      }
    }
	
   }
}

void monster_t::moveMonster(){

  bool tunneler = (attr>>2) & 0x01;
  
    if(d->checkHardness(npos)){
      if(tunneler){
	d->decreaseHardness(npos,85);
	if(d->checkHardness(npos)){
	  npos = pos;
	}
	else{
	  pos = npos;
	}
      }
    }
    else{
      pos = npos;
    }
  
  
}
void monster_t::setRandomPos(){
  
  std::vector<Room> Rooms = d->getRooms();
  int randomRoom = rand()%Rooms.size();
  pos = Rooms.at(randomRoom).getRandomPosition();
}


bool monster_t::checkLos(){
  position_t checkPos = pos;
  while(!(checkPos==pos)){
    int dx = player->getPos().x() - checkPos.x();
    int dy = player->getPos().y() - checkPos.y();
    dx = dx < 0 ? -1: dx == 0 ? 0 : 1;
    dy = dy < 0 ? -1: dy == 0 ? 0 : 1;
    checkPos = checkPos + position_t(dx,dy);
    if(d->checkHardness(checkPos)>0){
      seen = false;
      return false;
    }
  }
  dest = player->getPos();
  seen = true;

  
  return true;
}

void monster_t::parseMonster(){
  	std::cout<<"Name: " << _name<<std::endl;
	std::cout<<"Description: "<<std::endl;
	std::cout<<_desc<<std::endl;
	std::cout<< "Symbol:\t " << getSym() <<std::endl;
	std::cout<<"Color:\t "<<_color<<" ( " << index2color(_color) << " ) "<<std::endl;
	std::cout<<"Speed:\t "<<speed<< " ( " <<_speedDice.info << " )"<<std::endl;
	std::cout<<"Abilities:\t "<<std::hex<< "0x"<< attr<< " " << index2ability(attr)<<std::dec<<std::endl;
	std::cout<<"Hit Points:\t "<< _hp<< " ( " <<_hpDice.info << " )"<<std::endl;
	std::cout<<"Damage:\t "<< _dam.roll()<< " ( " <<_dam.info << " )"<<std::endl;
	std::cout<<std::endl;
}
