#include "player.h"

player_t::player_t(){
  _dam = dice(4,1,1);
  _maxhp = 20000;
  _hp = 20000;
  _def = 0;
  _hit = 0;
  _dodge = 0;
}

player_t::~player_t(){
}

player_t::player_t(Dungeon* dungeon, int spd) : player_t(){
  _isAlive = true;
  d = dungeon;
  _sight = 16;
  resetMap();
  setSym('@');
  setRandomPos();
  speed = spd;
  setEventTime();
  
}

void player_t::resetMap(){
  memset(seenMap, ' ',sizeof(char)*MAX_H*MAX_V);
}
void player_t::setRandomPos(){
  
  std::vector<Room> Rooms = d->getRooms();
  int randomRoom = rand()%Rooms.size();
  pos = Rooms.at(randomRoom).getRandomPosition();
  npos = pos;
  movePlayer();
}

int player_t::playerCommand(int k){
  int key = k;
  while(!validKeyCheck(key) || (npos.x()<1 || npos.x()>(MAX_H-2) || npos.y()< 1 || npos.y() >(MAX_V-2))){

	//key = wgetch(console->getGameWindow());
		
	npos = pos + getnewDirection(translateKeyinput(key));
	
	if(d->checkHardness(npos)!=0){
	  if(key!='t' && key!='f' && key!='5' && key!=' ' && key!='Q'){
	    key = 0;
	  }
	}
  }
  return key;
}




int player_t::movePlayer(){
  
  bool createMap = !(pos==npos);
  pos = npos;
  if(createMap){
    std::thread t1(&Dungeon::createTunnelMap, d, pos);
    std::thread t2(&Dungeon::createNonTunnelMap, d, pos);
    //d->createTunnelMap(pos);
    //d->createNonTunnelMap(pos);
    t1.join();
    t2.join();
  }
  int r = std::sqrt((double)_sight);
  for(int i =-r;i<=r;i++){
      for(int j=-r; j<=r;j++){
		position_t sPos = pos + position_t(i,j);
		position_t delta_pos = sPos - pos;
		if(checkLos(sPos) && delta_pos*delta_pos <=_sight){
		  seenMap[sPos.y()][sPos.x()] = d->getSym(sPos);
		}
      }
  }
  
  return 1;
}

bool player_t::checkLos(position_t check){
  position_t checkPos = pos;
  
      
  while(!(checkPos==check)){
    int dx = check.x() - checkPos.x();
    int dy = check.y() - checkPos.y();
    dx = dx < 0 ? -1: dx > 0;
    dy = dy < 0 ? -1: dy > 0;
    checkPos = checkPos + position_t(dx,dy);
    if(d->checkHardness(checkPos)!=0){
      return false;
    }
  }
  
  return true;
}


bool player_t::validKeyCheck(int key){
  char check[] = "*+egifm<>123456789Qykulnjbh ";
  int size = strlen(check);
  int i; 
  for(i = 0; i<size;i++){
    if(check[i]==(unsigned char)(key)){
      return true;
    }
  }
  return false;
}

int player_t::translateKeyinput(int key){
  int val = 9;
  switch(key){
    case 49:
      val = 0;
      break;
    case 50:
      val = 1;
      break;
    case 51:
      val = 2;
      break;
    case 52:
      val = 7;
      break;
    case 54:
      val = 3;
      break;
    case 55:
      val = 6;
      break;
    case 56:
      val = 5;
      break;
    case 57:
      val = 4;
      break;
    case 'b':
      val = 0;
      break;
    case 'j':
      val = 1;
      break;
    case 'n':
      val = 2;
      break;
    case 'h':
      val = 7;
      break;
    case 'l':
      val = 3;
      break;
    case 'y':
      val = 6;
      break;
    case 'k':
      val = 5;
      break;
    case 'u':
      val = 4;
      break;
      
    default:
      break;
  }

  return val;
}

void player_t::calculateStats(){
  speed = 10;
  //_hp = 2000;
  _def = 0;
  _hit = 0;
  _dodge = 0;
  _sight = 16;
  std::map<std::string,object>::iterator itr;
  for(itr = equipment.begin(); itr!=equipment.end();itr++){
    _def+=itr->second._def;
    speed+=itr->second._speed;
    _dodge+=itr->second._dodge;
    _hit+=itr->second._hit;
    
    if(itr->second._type&0x1<<10){
    
      _sight+=(itr->second._attr*itr->second._attr);
    }
    
  }
  speed = speed<0?1:speed;
}