#include "object.h"

object::object(){
  
  _hit = 0;
  _dodge = 0;
  _def = 0;
  _weight = 0;
  _speed = 0;
  _attr = 0;
  _val = 0;
  _dam = dice(0,0,0);
  _color = 0;
  _sym = '*';
}
object::~object(){
}

object::object(std::vector<std::string> arg) : object(){
  
  /*
  0 std::string _name;
  1 std::string _desc;
  2 int _type;
  3 int _color;
  4 dice _hit;
  5 dice _dam;
  6 dice _dodge;
  7 dice _def;
  8 dice _weight;
  9 dice _speed;
  10 dice _attr;
  11 dice _val;
  */
  
  if(arg.size() == 12){
  _name = arg.at(0);
  _desc = arg.at(1);
  int p;
  std::string temp = _desc;
  while ((p = temp.find("\n")) != std::string::npos) {
	  std::string t = temp.substr(0, p);
	  _descList.push_back(t);
	  temp.erase(0, p + 1);
  }

  _type = type2index(arg.at(2));
  _color = color2index(arg.at(3));
  parseColor(arg.at(3));
  _hitDice = dice(arg.at(4));
  _hit = _hitDice.roll();
  _dam = dice(arg.at(5));
  _dodgeDice = dice(arg.at(6));
  _dodge = _dodgeDice.roll();
  _defDice = dice(arg.at(7));
  _def = _defDice.roll();
  _weightDice = dice(arg.at(8));
  _weight = _weightDice.roll();
  _speedDice = dice(arg.at(9)); 
  _speed = _speedDice.roll();
  _attrDice = dice(arg.at(10));
  _attr = _attrDice.roll();
  _valDice = dice(arg.at(11));
  _val = _valDice.roll();
  setSym();
  }
}

void object::setGame(Dungeon * dungeon){

  d = dungeon;
}

void object::reRoll(){
  _hit = _hitDice.roll();
  _dodge = _dodgeDice.roll();
  _def = _defDice.roll();
  _weight = _weightDice.roll();
  _speed = _speedDice.roll();
  _attr = _attrDice.roll();
  _val = _valDice.roll();
}

void object::setRandomPos(){
  
  std::vector<Room> Rooms = d->getRooms();
  int randomRoom = rand()%Rooms.size();
  _pos = Rooms.at(randomRoom).getRandomPosition();
}
void object::setPos(position_t pos){
  _pos = pos;
}
int object::parseColor(std::string c){
    _color = color2index(c);
    for(int i = 0; i<8;i++){
      if(_color&(0x1<<i)){
	_colorIndex.push_back(i);
      }
    }
  return _color;
}

position_t object::getPos(){
  return _pos;
}
void object::parseObject(){
	/*
	0 std::string _name;
	1 std::string _desc;
	2 int _type;
	3 int _color;
	4 dice _hit;
	5 dice _dam;
	6 dice _dodge;
	7 dice _def;
	8 dice _weight;
	9 dice _speed;
	10 dice _attr;
	11 dice _val;
	*/
  	std::cout<<"Name: " << _name<<std::endl;
	std::cout<<"Description: "<<std::endl;
	std::cout<<_desc<<std::endl;
	std::cout<< "Type:\t " << _type<<" ( " << index2type(_type) << " ) "<<std::endl;
	std::cout<<"Color:\t "<<_color<<" ( " << index2color(_color) << " ) "<<std::endl;
	std::cout<<"Hit:\t "<<_hit<< " ( " <<_hitDice.info << " )"<<std::endl;
	std::cout<<"Damage:\t "<<_dam.roll()<< " ( " <<_dam.info << " )"<<std::endl;
	std::cout<<"Dodge:\t "<<_dodge<< " ( " <<_dodgeDice.info << " )"<<std::endl;	
	std::cout<<"Def:\t "<<_def<< " ( " <<_defDice.info << " )"<<std::endl;
	std::cout<<"Weight:\t "<<_weight<< " ( " <<_weightDice.info << " )"<<std::endl;
	std::cout<<"Speed:\t "<<_speed<< " ( " <<_speedDice.info << " )"<<std::endl;
	std::cout<<"Attr:\t "<<_attr<< " ( " <<_attrDice.info << " )"<<std::endl;
	std::cout<<"Val:\t "<<_val<< " ( " <<_valDice.info << " )"<<std::endl;
	std::cout<<"Sym:\t" <<_sym<<std::endl;
	std::cout<<std::endl;
	
}


void object::setSym(){

  char index[22] = "*|)}[]({\\=\"_~?!$/,-%&"; 

  for(int i = 0;i<20;i++){ 
    if(_type&(0x1<<i)){
      _sym = index[i+1];
      return;
    }
  }
  _sym = index[0]; 
}

void object::renderObject(){

  
}