#ifndef PLAYER_H
#define PLAYER_H
#include "character.h"
#include "dungeon.h"
#include "util.h"
#include <cmath>
class player_t : public character_t{
public:
   player_t();
   ~player_t();
   player_t(Dungeon * dungeon, int spd);
   void setRandomPos();
   int movePlayer();
   int playerCommand(int key);
   int translateKeyinput(int key);
   bool validKeyCheck(int key);
   char seenMap[MAX_V][MAX_H];
   bool checkLos(position_t check);
   void resetMap();
   void calculateStats();
private:
  Dungeon* d;
protected:
 
};



#endif