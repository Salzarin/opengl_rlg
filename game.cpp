#include "game.h"






Flags::Flags(){
  
  _NumberMonsters = 5;
  _Max_Rooms = 5;
  _save = false;
  _load = false;
  _player_auto = false;
  _player_speed = 10;
  std::string _filename = "";
}

Flags::~Flags(){
  
  
}

Flags::Flags(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms){
  _NumberMonsters = nummon;
  _Max_Rooms = max_rooms;
  _save = s;
  _load = l;
  _player_auto = a;
  _player_speed = ps;
  _verbose = v;
  _filename = fn;
}

Flags::Flags(const Flags& f){
    _NumberMonsters = f._NumberMonsters;
  _Max_Rooms = f._Max_Rooms;
  _save = f._save;
  _load = f._load;
  _player_auto = f._player_auto;
  _player_speed = f._player_speed;
  _verbose = f._verbose;
  _filename = f._filename;
}




Game::Game(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, bool checkParser,std::string mfilename, std::string ofilename, int max_rooms) : Game(){
  
  if(!checkParser){
    checkParse = false;
    flag = new Flags(fn,v,l,s,a,ps,nummon,max_rooms);
    _mFile = mfilename;
    _oFile = ofilename;
    ParseIsGood = parseFile(_mFile, _oFile) ==0;
    if(ParseIsGood){
      gameInit();
    }
  }
  else{
    checkParse = true;
    parseFile(mfilename, ofilename);
  }
  
}


Game::Game(){
  

}

Game::~Game(){

  delete d;
  delete p;
  delete flag;
  deleteMonsters();
  delete mList;
}

unsigned int Game::getElapsedTime(){  
  return (int)std::chrono::duration_cast<std::chrono::seconds>
  (std::chrono::system_clock::now() - startTime).count();
}

unsigned int Game::getRenderTime(){  
  return (int)std::chrono::duration_cast<std::chrono::microseconds>
  (std::chrono::high_resolution_clock::now() - renderTime).count();
}

unsigned int Game::getLoopTime(){  
  return (int)std::chrono::duration_cast<std::chrono::microseconds>
  (std::chrono::high_resolution_clock::now() - loopTime).count();
}

void Game::deleteMonsters(){
  std::vector<character_t*>::iterator itr;
  for(itr = mList->begin();itr!=mList->end();itr++){
    delete (*itr);
    (*itr)=NULL;
  }
  mList->clear();
}

int Game::parseMonsters(std::string mfilename){

  std::string _mfilename = mfilename;
  
  std::cout<<std::endl<<std::endl;
  if(_mfilename.length() == 0){
    _mfilename = std::string("./data/")+
		     std::string("monster_desc.txt");
  }
  std::cout<< "Loading file " << _mfilename <<std::endl;
  
  std::ifstream mloadfile(_mfilename.c_str());
  
  if(!_mfilename.length()){
    printf("No File");
    return -1;
  }
  if(!mloadfile.good()){
    //printf("File Error : %s : %s\n", _mfilename.c_str(),strerror_s(errno));
    return -1;  
  }
  std::string temp;
  std::getline(mloadfile,temp);
  if(temp.compare(std::string("RLG327 MONSTER DESCRIPTION 1\n"))){
    
    //std::cout<<"Monster Data Found"<<std::endl;
    
    int na, de, co, sp, ab, h, da, sy;
    int numerrors = 0;
    int numcorrect = 0;
    while(!mloadfile.eof()){
      int i = temp.find("END");
      int j = temp.find("BEGIN MONSTER");
      na = de = co = sp = ab = h = da = sy=0;
      std::string name, desc, color, speed, abil, hp, dam, symbol;
      while(i == -1){
	std::getline(mloadfile,temp);
	
	
	if(temp.find("NAME") == 0){
	  name =  getParsedData(temp, std::string("NAME"),na);
	  na++;
	}
	if(temp.find("DESC") == 0){
	  std::getline(mloadfile,temp);
	  while(checkMonsterFields(temp) == -1 && temp.find(".") != 0 && temp.length() !=1){
	    desc += temp + "\n";
	    std::getline(mloadfile,temp);
	      
	  }
	  if(temp.find(".") == 0){
	    de++;
	  }
	}
	if(temp.find("SYMB") == 0){
	  symbol =  getParsedData(temp, std::string("SYMB"),sy);
	  sy++;
	}
	if(temp.find("COLOR") == 0){
	  color =  getParsedData(temp, std::string("COLOR"),co);
	  co++;
	}
	if(temp.find("SPEED") == 0){
	  speed =  getParsedData(temp, std::string("SPEED"),sp);
	  sp++;
	}
	if(temp.find("ABIL") == 0){
	  abil =  getParsedData(temp, std::string("ABIL"),ab);
	  ab++;
	}
	if(temp.find("HP") == 0){
	  hp =  getParsedData(temp, std::string("HP"),h);
	  h++;
	}
	if(temp.find("DAM") == 0){
	  dam =  getParsedData(temp, std::string("DAM"),da);
	  da++;
	}
	i = temp.find("END");
	j = temp.find("BEGIN MONSTER");
	if(j!=-1){
	  i = -1;
	}
      }
      
      if(na*de*co*sp*ab*h*da*sy==1){
	monsterTypes.push_back(monster_t(name,desc,symbol,color,speed,abil,hp,dam));
	numcorrect++;
      }
      else{
      numerrors++;
      }
      
    //std::cout<<"Checking Next Monster"<<std::endl<<std::endl;
    std::getline(mloadfile,temp);
      
      
    }
    std::cout<<"Found "<<numcorrect<<" Monsters and "<<numerrors << " Parsing Errors."<<std::endl; 
    if(checkParse){
      
      std::cout<<"Checking Monster Parsing."<<std::endl<<std::endl;
      
      std::vector<monster_t>::iterator itr;
      
      for(itr=monsterTypes.begin();itr!=monsterTypes.end();itr++){
	(*itr).parseMonster();
      }    
    }
  }
  return 0;
}

int Game::parseObjects( std::string ofilename){
  
  std::cout<<std::endl<<std::endl;
  
  std::string _ofilename = ofilename;
  
  if(_ofilename.length() == 0){
    _ofilename = std::string("./data/") +
		     std::string("object_desc.txt");
  }
  
  
  std::cout<< "Loading file " << _ofilename <<std::endl;
  
  std::ifstream oloadfile(_ofilename.c_str());
  
  if(!_ofilename.length()){
    printf("No File");
    return -1;
  }
  if(!oloadfile.good()){
    //printf("File Error : %s : %s\n", _ofilename.c_str(),strerror(errno));
    return -1;  
  }
  std::string temp;
  std::getline(oloadfile,temp);
  if(temp.compare(std::string("RLG327 OBJECT DESCRIPTION 1\n"))){
    
    //std::cout<<"Object Data Found"<<std::endl;
    
    //std::vector<object> objects;
    int na,de,ty,co,hi,da,dod,d,we,sp,at,va;
    int numerrors = 0;
    int numcorrect = 0;
    while(!oloadfile.eof()){
      int i = temp.find("END");
      int j = temp.find("BEGIN OBJECT");
      na=de=ty=co=hi=da=dod=d=we=sp=at=va = 0;
      std::string name, desc, type, color, hit, dam, dodge, def, weight, speed, attr, val;
      while(i == -1){
	std::getline(oloadfile,temp);
	
	if(temp.find("NAME") == 0){
	  name =  getParsedData(temp, std::string("NAME"),na);
	  na++;
	}
	if(temp.find("DESC") == 0){
	  std::getline(oloadfile,temp);
	  while(checkMonsterFields(temp) == -1 && temp.find(".") != 0 && temp.length() !=1){
	    desc += temp + "\n";
	    std::getline(oloadfile,temp);     
	  }
	  if(temp.find(".") == 0){
	    de++;
	  }
	}
	if(temp.find("TYPE") == 0){
	  type =  getParsedData(temp, std::string("TYPE"),ty);
	  ty++;
	}
	if(temp.find("COLOR") == 0){
	  color =  getParsedData(temp, std::string("COLOR"),co);
	  co++;
	}
	if(temp.find("HIT") == 0){
	  hit =  getParsedData(temp, std::string("HIT"),hi);
	  hi++;
	}
	if(temp.find("DAM") == 0){
	  dam =  getParsedData(temp, std::string("DAM"),da);
	  da++;
	}
	if(temp.find("DODGE") == 0){
	  dodge =  getParsedData(temp, std::string("DODGE"),dod);
	  dod++;
	}
	if(temp.find("DEF") == 0){
	  def =  getParsedData(temp, std::string("DEF"),d);
	  d++;
	}
	if(temp.find("WEIGHT") == 0){
	  weight =  getParsedData(temp, std::string("WEIGHT"),we);
	  we++;
	}
	if(temp.find("SPEED") == 0){
	  speed =  getParsedData(temp, std::string("SPEED"),sp);
	  sp++;
	}
	if(temp.find("ATTR") == 0){
	  attr =  getParsedData(temp, std::string("ATTR"),at);
	  at++;
	}
	if(temp.find("VAL") == 0){
	  val =  getParsedData(temp, std::string("VAL"),va);
	  va++;
	}
	
	i = temp.find("END");
	j = temp.find("BEGIN OBJECT");
	if(j!=-1){
	  i = -1;
	}
      }
      
      if(na*de*ty*co*hi*da*dod*d*we*sp*at*va==1){
	std::vector<std::string> arg;
	arg.push_back(name);
	arg.push_back(desc);
	arg.push_back(type);
	arg.push_back(color);
	arg.push_back(hit);
	arg.push_back(dam);
	arg.push_back(dodge);
	arg.push_back(def);
	arg.push_back(weight);
	arg.push_back(speed);
	arg.push_back(attr);
	arg.push_back(val);

	objectTypes.push_back(object(arg));
	numcorrect++;
      }
      else{
      numerrors++;
      }
      
      
      
      //std::cout<<"Checking Next Object"<<std::endl<<std::endl;
      std::getline(oloadfile,temp);
      
      
    }
    
    std::cout<<"Found "<<numcorrect<<" Objects and "<<numerrors << " Parsing Errors."<<std::endl<<std::endl;; 
    if(checkParse){
      std::cout<<"Checking Object Parsing."<<std::endl<<std::endl;
      std::vector<object>::iterator itr;
      
      for(itr=objectTypes.begin();itr!=objectTypes.end();itr++){
	(*itr).parseObject();
      }
    }
  }
  
  
  return 0;
}


int Game::parseFile(std::string mfilename, std::string ofilename){
  
  
  int parseM = parseMonsters(mfilename);
  int parseO = parseObjects(ofilename);
  
  if(parseM == -1 || parseO==-1){
    return !(parseM == -1 || parseO==-1);
  }
  
  objectTypes.front().setSym();
  
 return 0; 
}
void Game::generateMonsters(){
  bool monsterMap[MAX_V][MAX_H];
  memset(monsterMap,false,sizeof(monsterMap));
  character_map.clear();
  deleteMonsters();
  int i;
  std::vector<std::string> unique;
  monsterMap[d->getStairs(0).y()][d->getStairs(0).x()] = true;
  monsterMap[d->getStairs(1).y()][d->getStairs(1).x()] = true;
  for(i = 0; i<flag->_NumberMonsters;i++){ 
    
    
    mList->push_back(new monster_t());
    
    *(mList->back()) = monsterTypes.at(rand()%monsterTypes.size());
    bool check = std::find(unique.begin(),unique.end(),mList->back()->_name)!=unique.end();
    
    
    while(check || (!mList->back()->_isAlive && ((0x1<<7)&mList->back()->attr))){
      *(mList->back()) = monsterTypes.at(rand()%monsterTypes.size());
      check = std::find(unique.begin(),unique.end(),mList->back()->_name)!=unique.end();
    }
    
    if(((0x1<<7)&mList->back()->attr) && mList->back()->_isAlive){
      unique.push_back(mList->back()->_name);
      
    }
   
    
    ((monster_t*)mList->back())->setGame(p,d);
    
    if(((monster_t *)mList->back())->initMonsterPosition(monsterMap)){
      eventQueue.push(mList->back());
      monsterMap[mList->back()->getPos().y()][mList->back()->getPos().x()]=true;  
    }
    else{
      delete mList->back();
      mList->pop_back();
      break;
    }
  }
  
  std::vector<character_t*>::iterator m_itr;
  for(m_itr = mList->begin();m_itr!=mList->end();m_itr++){
    insertCharacter((*m_itr)->getPos(),*m_itr);
  }
  
  
}

void Game::generateObjects(){
 
  object_map.clear();
  bool onStairs = false;
  for(int i = 0; i<50;i++){
	std::map<std::pair<int, int>, character_t*>::iterator c_itr;
    object new_object; 
    do{
    new_object = objectTypes.at(rand()%objectTypes.size());
    new_object.setGame(d);
    new_object.setRandomPos();
    new_object.reRoll();
    //Check if It lands on a character space.
    c_itr = checkCharacter(new_object.getPos());

    onStairs = d->checkStairs(new_object.getPos());
    
    
	} while (c_itr != character_map.end() || onStairs);
    insertObject(new_object.getPos(), new_object);
    
  }
  
  renderingThread->setObjectMap(&object_map);
}




void Game::resetEventList(){
  eventQueue.clear();
  std::vector<character_t*>::iterator itr;
  for(itr = mList->begin();itr!=mList->end();itr++){
   eventQueue.push((*itr));
  }
}


void Game::setKey(int _k) {
	_key = _k;
}
int Game::getKey() {
	int value=0; 
	while (value == 0 &&glfwWindowShouldClose(window) == 0) {		
		value = _key;
	}

	

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) != 0) {
		value = 'Q';
	}
	_key = 0;

	return value;
}

void charParse(GLFWwindow* window, unsigned int codepoint, int mods) {
	Game * game = reinterpret_cast<Game *>(glfwGetWindowUserPointer(window));
	game->setKey(codepoint);
}

void keyParse(GLFWwindow* window, int codepoint, int scancode, int action, int mods){
	
	Game * game = reinterpret_cast<Game *>(glfwGetWindowUserPointer(window));
	if (action == GLFW_PRESS) {
		switch (codepoint) {
		case GLFW_KEY_ESCAPE:
			game->setKey(codepoint);
			break;
		case GLFW_KEY_UP:
			game->setKey(codepoint);
			break;
		case GLFW_KEY_DOWN:
			game->setKey(codepoint);
			break;
		case GLFW_KEY_ENTER:
			game->setKey(codepoint);
			break;
		case GLFW_KEY_KP_ENTER:
			game->setKey(GLFW_KEY_ENTER);
			break;
		default:
			break;
		}
	}
}

void Game::gameInit(){
  
  renderingThread = new oglRender();
  
  
  _reveal = false;
  teleporting = false;
  displayMonsterList = false;
  displayEquipment = false;
  startTime = std::chrono::system_clock::now();
  d = new Dungeon(flag->_Max_Rooms, flag->_save, flag->_load, flag->_filename);
  d->loadDungeon("./data/adventure.rlg327");
  p = new player_t(d, flag->_player_speed);
  mList = new std::vector<character_t*>;

  p->setEventTime(0);
  eventQueue.push(p);
  
 
  displayItemList = false;
  displayItemDescription = false;
  displayMonsterDescription = false;
  _revealed = false;


  displayEquipmentList = false;
  monsterLook=false;
  
  BossDead = false;
  monstersDead = 0;
  generateMonsters();
  insertCharacter(p->getPos(),p);
  generateObjects();
  godMode = false;
  
}


void Game::teleportMode(){
  teleporting = true;
  int key = 0;
  teleport = p->getPos();
  renderingThread->setCursor(teleport);

  while(key!='g' && key!='r'){
      position_t newTeleport = position_t(0,0);
    while((!p->validKeyCheck(key) && (key!='r')) || ((newTeleport.x()<1 || newTeleport.x()>MAX_H-2 || newTeleport.y()<1 || newTeleport.y()>MAX_V-2))){

		key = getKey();
		if(key =='r'){
		  teleport = position_t(rand()%MAX_H,rand()%MAX_V);
		  while(d->checkHardness(teleport)){
			teleport = position_t(rand()%MAX_H,rand()%MAX_V);
		  }
		}
		newTeleport = teleport + getnewDirection(p->translateKeyinput(key));	
    }
	
	teleport = newTeleport;
	renderingThread->setCursor(newTeleport);
  }
  teleporting = false;
  p->npos = teleport;
}

void Game::resetMap(){
  
  d->createDungeon();
  generateMonsters();
  resetEventList();
  generateObjects();
  
}
int Game::playerCommand(int k) {
	int key = k;
	while (!p->validKeyCheck(key) || (p->npos.x()<1 || p->npos.x()>(MAX_H - 2) || p->npos.y()< 1 || p->npos.y() >(MAX_V - 2))) {

		//key = wgetch(console->getGameWindow());
		key = getKey();
		p->npos = p->pos + getnewDirection(p->translateKeyinput(key));

		if (d->checkHardness(p->npos) != 0) {
			if (key != 't' && key != 'f' && key != '5' && key != ' ' && key != 'Q') {
				key = 0;
			}
		}
	}
	return key;
}

void Game::listItems(int type) {


	std::string keyCheck = "iem";
	char key_type = keyCheck[type];
	listStart = 0;
	listSelect = 0;
	int size = 12;
	//int list_size = 10;
	int key = 0;
	renderingThread->setItemSelection(listSelect);
	switch (key_type) {
		case 'i':
			list_size = p->inventory.size();
			break;
		case 'e':
			list_size = 12;
			break;
		case 'm':
			list_size = mList->size();
			break;
		default:
			key = GLFW_KEY_ESCAPE;
			break;
	}

	while (key != GLFW_KEY_ESCAPE && key != 'Q' && key != key_type) {

		key = getKey();
		if (key == '8' || key == GLFW_KEY_UP) {
			listSelect--;
			listSelect = listSelect<0 ? 0 : listSelect;
			if (listSelect<listStart) {
				if (listStart>0) {
					listStart--;
				}
			}
			renderingThread->setItemSelection(listSelect);
		}

		if (key == '2' || key == GLFW_KEY_DOWN) {
			listSelect++;
			listSelect = listSelect>(size - 1) ? (size - 1) : listSelect;
			if (key_type == 'm') {
				listSelect = listSelect>(list_size-1) ? (list_size-1) : listSelect;
				listSelect = listSelect <= 0 ? 0 : listSelect;
			}
			if (listSelect>(listStart + list_size)) {
				if ((listStart + list_size)<size) {
					listStart++;
				}
			}
			renderingThread->setItemSelection(listSelect);
		}

		if (displayItemList) {
			if (key == GLFW_KEY_ENTER) {
				if ((int)p->inventory.size() > listSelect) {
					p->equipItem(listSelect);
					p->calculateStats();
					list_size = p->inventory.size();
				}
			}
			if (key == 'd') {
				if (listSelect < (int)p->inventory.size()) {
					object dropped_item = p->dropItem(listSelect);
					insertObject(p->getPos(), dropped_item);
				}
			}
			if (key == 'x') {
				if (listSelect < (int)p->inventory.size()) {
					object dropped_item = p->dropItem(listSelect);
				}

			}
		}
		if (displayEquipment) {
			if (key == GLFW_KEY_ENTER) {
				if (list_size > listSelect)
				{
					
					object dropped_item = p->unequipItem(listSelect);
					if (dropped_item._type != 0) {
						insertObject(p->getPos(), dropped_item);
					}
					p->calculateStats();
					//list_size = p->inventory.size();
				}

			}
		}
	}
}


void Game::gameLoop(){
  int key = 0;
  mtx = new std::mutex;
  //Rendering = true;
  renderingThread->setDungeon(d, p, mList);
  renderingThread->mtx = mtx;
  renderThread = std::thread(&oglRender::Render, renderingThread);
  while (!renderingThread->getRender());

  window = renderingThread->getWindow();

  glfwMakeContextCurrent(window);

  glfwSetWindowUserPointer(window, this);
  glfwSetCharModsCallback(window,charParse);
  glfwSetKeyCallback(window, keyParse);
  
  renderingThread->setReveal(true);
  key = 0;
 
  //Intro Screen
  renderingThread->statusMessage = "RLG327: Press Enter to Start Playing.\n";
  while (glfwWindowShouldClose(window) == 0 && _key != GLFW_KEY_ENTER && _key != GLFW_KEY_KP_ENTER && p->getAlive()) {
	  
  }

  renderingThread->setReveal(false);
  
  resetMap();
  eraseCharacter(p->getPos());
  p->setRandomPos();
  p->setEventTime(0);
  p->resetMap();
  p->movePlayer();
  insertCharacter(p->getPos(), p);
  eventQueue.push(p);
  renderingThread->gameState = 0;
  _key = 0;
  while(glfwWindowShouldClose(window) == 0 && key!='Q' && p->getAlive()){
  
	  eventQueue.buildHeap();
    //Get Next Character
	character_t* character = eventQueue.pop();
    
    if(!character){
      continue;
    }

    
	//std::cout << character->_name << std::endl;
      eventQueue.decreaseKey(character->getEventTime());

    
    
    
    key = 0;
    int waitTime = character->getEventTime(); 
    loopTime = std::chrono::high_resolution_clock::now();
    
    if(character->getSym() == '@'){
      //Handle Player Behaviour
	  
	  
	  //key = getKey();
      eraseCharacter(character->getPos());
      p->setEventTime();
      key = playerCommand(key);
      mtx->lock();
    if(key == '<' || key == '>'){
	//Reset Map
		if(d->checkMap(p->getPos()) == '<' && key=='<'){
		  resetMap();
		  p->setPos(d->getStairs(1));
		  p->resetMap();
		}
		if(d->checkMap(p->getPos()) == '>'&& key=='>'){
		  resetMap();
		  p->setPos(d->getStairs(0));
		  p->resetMap();
		}
		p->setEventTime(0);
      }
      
      if(key == '*'){
		godMode=!godMode;
		p->setEventTime(0);
      }
      
      if(key == '+'){
		p->_hp+= p->_maxhp/100;
		p->_hp = p->_hp>p->_maxhp?p->_maxhp:p->_hp;
      }
      
      if(key == 'm'){
		//Render Monster List
		renderingThread->displayMenu('m');
		listItems(2);
		renderingThread->displayMenu('m');
		p->setEventTime(0);  
		displayMonsterList = false;
      }
      
	  if (key == 'e') {
		  displayEquipment = true;
		  renderingThread->displayMenu('e');
		  listItems(1);
		  renderingThread->displayMenu('e');
		  displayEquipment = false;
	  }
      if(key == 'g'){
		//Teleport Mode
		renderingThread->toggleCursorMode();
		renderingThread->setReveal(true);
		teleportMode();
		renderingThread->setReveal(false);
		renderingThread->toggleCursorMode();
		p->setEventTime(0);

      }
      
      if(key == 'L'){
		//look Mode
		//lookMode();
		p->setEventTime(0);      
	
      }
      
      if(key == 'f'){
		  _revealed = !_revealed;
		  renderingThread->setReveal(_revealed);
		p->setEventTime(0);
      }
     
	  if (key == 'i') {
		displayItemList = true;
		renderingThread->displayMenu('i');
		listItems(0);
		renderingThread->displayMenu('i');
		displayItemList = false;
		p->setEventTime(0);
	  }
      
	  std::map<std::pair<int, int>, character_t*>::iterator mCheck = checkCharacter(p->getNPos());

      if(mCheck!=character_map.end()){
	    
	    int damage = p->damageRoll();
	    
	    bool critical_hit = false;
	    if(p->_hit>0){
	      critical_hit = (rand()%p->_hit)>50;
	    }
	    int multiplier = critical_hit?10:1;
	    
	    if(godMode){
			damage = mCheck->second->_hp;//mCheck.first->second->_hp;
	    }
	    
	    bool monsterState = mCheck->second->decreaseHp(damage*multiplier);
	    if(monsterState){
	      p->npos =p->pos;
	    }
	    if(!critical_hit){
			renderingThread->setDebugMessage("You Hit " + mCheck->second->_name + " for " +std::to_string(damage*multiplier)+ "!");
	    }
	    else{
			renderingThread->setDebugMessage("You Critically Hit " + mCheck->second->_name + " for " +std::to_string(damage*multiplier) + "!");
	    }
	    
	      //console->print_dinfo("HP : " + std::to_string(p->_hp));
		
		//Kill Monster Routine
		if(!monsterState){
		  if(!mCheck->second->_name.compare("SpongeBob SquarePants")){
			BossDead = true;
			mtx->unlock();
			break;
		  }
		  monstersDead++;
		  std::vector<character_t*>::iterator mitr;
		  mitr = std::find(mList->begin(),mList->end(), mCheck->second);
		  if((*mitr)->attr & 0x1<<7){
			std::vector<monster_t>::iterator t_itr;
			for(t_itr = monsterTypes.begin();t_itr!=monsterTypes.end();t_itr++){
			  if(!(*mitr)->_name.compare((*t_itr)._name)){
			(*t_itr)._isAlive = false;
			  }
			}
		  }
	  
		  eraseCharacter((*mitr)->getPos());
		  delete (*mitr);
		  (*mitr) =NULL;
		  mList->erase(mitr);
		  resetEventList();
		}
	
      }
      
      
      if(!(p->getNPos()==p->getPos())){ //Only Pick up if Position Changed
	
		//Check if Object is in next space
		oMap::iterator o_itr = checkObject(p->getNPos());
		if(o_itr!=object_map.end()){
	  
			//check item slots	
			while(p->inventory.size() < 10 && o_itr->second.size()){
			p->addItem(popObject(p->getNPos()));
			}
	  
			if(!o_itr->second.size()){
				eraseObject(p->getNPos());
				renderingThread->setObjectMap(&object_map);
			}
		}
      }

      //Move the Player
      p->movePlayer();
      insertCharacter(p->getPos(), p);
      eventQueue.push(p);
      mtx->unlock();
    }
    else{
	mtx->lock();
		monster_t* m = dynamic_cast<monster_t*>(character);
		if(m){
		  eraseCharacter(m->getPos());
	  
		  m->monsterNextMove();
	  
		  //Check If Player
		  if(m->npos== p->getPos()){
			//Combat
			int damage = m->damageRoll();
			damage-=p->_def/4;
			damage = damage<0?0:damage;
			//damage = godMode?0:damage;
			bool playerState = p->getAlive();
			renderingThread->setDebugMessage(m->_name + " hit you for "+ std::to_string(damage) + " damage!");
			if(!godMode){
			  playerState = p->decreaseHp(damage);
			  p->setAlive(playerState);
			}
	    
			if(playerState){
			  m->npos =m->pos;
			}



		  }
	  
	  
		  //cMapItr mCheck = insertCharacter(m->getNPos(),m);
	  
		  cMap::iterator mCheck = checkCharacter(m->getNPos());
		  //Check If Monster
		  if(mCheck!=character_map.end()){
	    
		  
			 position_t delta = m->npos - m->pos;
	    
			int start_check = getDirectionIndex(delta);
			int j;
			position_t pos_check;
			cMap::iterator nmCheck;
			for(j = 0;j<8;j++){
			  pos_check = m->npos+getnewDirection((start_check+j)%8);
			  nmCheck = character_map.find(pos_check.getKey());
			  if(nmCheck==character_map.end() && !d->checkHardness(pos_check)){
				break;
			  }
			}
			if(nmCheck==character_map.end() && !d->checkHardness(pos_check)){
	      
			  mCheck->second->setPos(pos_check);
			  insertCharacter(mCheck->second->getPos(), mCheck->second);
			  eraseCharacter(m->getNPos());
			}
			else{
			  mCheck->second->setPos(m->getPos());
			  insertCharacter(mCheck->second->getPos(), mCheck->second);
			  eraseCharacter(m->getNPos());
			}
			//m->npos = m->pos;
	  
	    
	    
		  }


		  m->moveMonster();
		  insertCharacter(m->getPos(),m);
		  //key = wgetch(console->getGameWindow());
		  m->setEventTime();
		  eventQueue.push(m);
		}
	mtx->unlock();
    }

    
    
    int time = getLoopTime();
      //console->print_dinfo(std::to_string(character->equipment.size()));
      
 
    
  }
  
  
  //renderThread.join();
  GameOverScreen();
}



void Game::GameOverScreen(){

	int key = 0;
	std::stringstream str;
    if(!BossDead){
      if(!p->_isAlive){
		  str << "You Died: Monsters Vanquished " << monstersDead<<std::endl;;
      }
      else{
		  str << "Way to Rage Quit: You had killed " << monstersDead << " monster(s)."<<std::endl;
      }
    }
    else{
		str << "You Win: Monsters Vanquished " << monstersDead << " plus a Dead Sponge" << std::endl;
    }
	str << "Press Q to quit." << std::endl;;
	key = 0;
	renderingThread->statusMessage = str.str();
	renderingThread->gameState = 1;
	while (key != 'Q' && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0) {
		key = getKey();
	}
	
	renderingThread->setRender(false);
	renderThread.join();
}




Game::cMapItr Game::insertCharacter(position_t pos, character_t* c){
  return character_map.insert(cPair(pos.getKey(),c));
}

std::map<std::pair<int, int>, character_t*>::iterator Game::checkCharacter(position_t pos) {
	return character_map.find(pos.getKey());
}


void Game::eraseCharacter(position_t pos){
  character_map.erase(pos.getKey());
}


Game::oMap::iterator Game::checkObject(position_t pos){
  oMap::iterator itr = object_map.find(pos.getKey());
  return itr;
}
Game::oMapItr Game::insertObject(position_t pos, object obj){
  oMapItr itr = object_map.insert(oPair(pos.getKey(),std::vector<object>()));
  obj.setPos(pos);
  itr.first->second.push_back(obj);
  return itr;
}


std::vector<object> Game::eraseObject(position_t pos){
  std::vector<object> objects = object_map.at(pos.getKey());
  object_map.erase(pos.getKey());
  return objects;
}

object Game::popObject(position_t pos){
  std::vector<object> objects = object_map.at(pos.getKey());
  object temp = objects.front();
  object_map.at(pos.getKey()).erase(object_map.at(pos.getKey()).begin());
  return temp;
}
