#include "room.h"


Room::Room(){
}

Room::~Room(){
}

Room::Room(int x, int y, int w, int h){

  pos = position_t(x,y);
  dim = dimension_t(w,h);
  cpos = pos + dim/2;
  setValid(false);
}

bool Room::checkRoomIntersection(Room B){

int buffer = 2;

//Add one to the bounds, so the bounding box
//guarantees that we have 1 space between rooms
int Aleft = x() - buffer;
int Atop = y()-buffer;
int Aright = x()+w()+buffer; 
int Abot = y()+h()+buffer;

int Bleft = B.x()-buffer;
int Btop = B.y()-buffer;
int Bright = B.x()+B.w()+buffer; 
int Bbot = B.y()+B.h()+buffer;

	//Bound Check, if any one of these are true,
	// no overlap can exist.
	if(Aright < Bleft) return false;
	if(Aleft > Bright) return false;
	if(Atop > Bbot) return false;
	if(Abot < Btop) return false;
	
				
	//We're returning True if it overlaps
	return true;
}


    bool Room::getValid(){
      return valid;
    }
void Room::setValid(bool set){
      valid = set;
}
int Room::x(){
    return pos.x();
}
int Room::y(){
    return pos.y();
}
int Room::cx(){
    return cpos.x();
}
int Room::cy(){
    return cpos.y();
}
int Room::right(){
    return (pos+dim).x();
}
int Room::bottom(){
    return (pos+dim).y();
}
int Room::w(){
    return dim.w();
}
int Room::h(){
    return dim.h();
} 


position_t Room::getRandomPosition(){
  int rw = rand() % dim.w();
  int rh = rand() % dim.h();
  dimension_t rdim = dimension_t(rw,rh);
  position_t position = pos + rdim;
  
  return position;
}

